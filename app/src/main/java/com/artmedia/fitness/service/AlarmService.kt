package com.artmedia.fitness.service

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import com.artmedia.fitness.data.model.ReminderModel
import com.artmedia.fitness.receiver.AlarmReceiver
import com.artmedia.fitness.util.Constants
import timber.log.Timber
import java.util.*

class AlarmService(private val context: Context) {
    private val alarmManager: AlarmManager? =
        context.getSystemService(Context.ALARM_SERVICE) as AlarmManager?

    // Every week
    fun setRepetitiveAlarm(timeInMillis: Long, requestCode: Int) {
        Timber.d("setAlarm $requestCode")
        setAlarm(
            timeInMillis,
            getPendingIntent(
                getIntent().apply {
                    action = Constants.ACTION_SET_REPETITIVE_ALARM
                    putExtra(Constants.EXTRA_EXACT_ALARM_TIME, timeInMillis)
                },
                requestCode
            )
        )

        val pendingIntent = PendingIntent.getBroadcast(
            context,
            requestCode,
            getIntent().apply {
                action = Constants.ACTION_SET_REPETITIVE_ALARM
                putExtra(Constants.EXTRA_EXACT_ALARM_TIME, timeInMillis)
            },
            PendingIntent.FLAG_NO_CREATE
        )
        Timber.d("pendingIntent: $pendingIntent")
    }

    private fun setAlarm(timeInMillis: Long, pendingIntent: PendingIntent) {
        alarmManager?.let {
            alarmManager.setRepeating(
                AlarmManager.RTC_WAKEUP,
                timeInMillis,
                AlarmManager.INTERVAL_DAY,
                pendingIntent
            )
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
//                alarmManager.setExactAndAllowWhileIdle(
//                    AlarmManager.RTC_WAKEUP,
//                    timeInMillis,
//                    pendingIntent
//                )
//            } else {
//                alarmManager.setExact(
//                    AlarmManager.RTC_WAKEUP,
//                    timeInMillis,
//                    pendingIntent
//                )
//            }
        }

    }

    fun cancelAlarm(reminder: ReminderModel, requestCode: Int) {
        Timber.d("cancel requestCode: $requestCode")
        val weekDays = intArrayOf(
            Calendar.MONDAY,
            Calendar.TUESDAY,
            Calendar.WEDNESDAY,
            Calendar.THURSDAY,
            Calendar.FRIDAY,
            Calendar.SATURDAY,
            Calendar.SUNDAY
        )
        val calendar: Calendar = Calendar.getInstance().apply {
            set(Calendar.DAY_OF_WEEK, weekDays[requestCode - 1])
            set(Calendar.HOUR_OF_DAY, reminder.hour)
            set(Calendar.MINUTE, reminder.minute)
            set(Calendar.SECOND, 0)
        }

        val intent = Intent(context, AlarmReceiver::class.java)
        intent.apply {
            action = Constants.ACTION_SET_REPETITIVE_ALARM
            putExtra(Constants.EXTRA_EXACT_ALARM_TIME, calendar.timeInMillis)
        }
        val pendingIntent = PendingIntent.getBroadcast(
            context,
            requestCode,
            intent,
            PendingIntent.FLAG_NO_CREATE
        )
        alarmManager?.cancel(pendingIntent)
        pendingIntent.cancel()
    }

    private fun getIntent() = Intent(context, AlarmReceiver::class.java)

    private fun getPendingIntent(intent: Intent, requestCode: Int) =
        PendingIntent.getBroadcast(
            context,
            requestCode,
            intent,
            PendingIntent.FLAG_UPDATE_CURRENT
        )
}



