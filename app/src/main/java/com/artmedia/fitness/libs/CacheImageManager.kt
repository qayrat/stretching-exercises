package com.artmedia.fitness.libs

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import timber.log.Timber
import java.io.*

class CacheImageManager {
    companion object {
        fun getImage(context: Context, imageName: String) : File? {
            val fileName = "${context.getExternalFilesDir(null)}/${imageName}"
            var file: File?=null
            var fileInputStream: FileInputStream? = null
            try {
                file = File(fileName)
                fileInputStream = FileInputStream(file)
            } catch (e: FileNotFoundException) {
                e.printStackTrace()
            }
            return file
//            val file = File(fileName)
//            Timber.d(file.absolutePath)
//            var bitmap: Bitmap? = null
//            try {
//                bitmap = BitmapFactory.decodeStream(FileInputStream(file))
//            } catch (e: FileNotFoundException) {
//                e.printStackTrace()
//            }
//            return bitmap
        }

        fun putImage(context: Context, imageName: String, file: File) {
            val fileName: String = "${context.getExternalFilesDir(null)}/${imageName}"
            val fileItem = File(fileName)
            var fileOutputStream: FileOutputStream? = null
            try {
                var byteSum = 0
                var byteRead = 0
                if (fileItem.exists()) {
                    val fileInputStream = FileInputStream(file)
                    fileOutputStream = FileOutputStream(fileItem)
                    val buffer = ByteArray(1444)
                    var length: Int
                    byteRead = fileInputStream.read(buffer)
                    while (byteRead != -1) {
                        byteSum += byteRead
                        byteRead = fileInputStream.read(buffer)
                        fileOutputStream.write(buffer, 0, byteRead)
                    }
                    fileInputStream.close()
                }
            } catch (e: FileNotFoundException) {
                e.printStackTrace()
            } finally {
                if (fileOutputStream != null) {
                    try {
                        fileOutputStream.close()
                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                }
            }
        }
    }
}