package com.artmedia.fitness.libs

import android.content.Context
import android.util.DisplayMetrics
import android.view.View
import androidx.fragment.app.FragmentActivity
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdSize
import com.google.android.gms.ads.AdView
import timber.log.Timber

class AdMobAdaptiveBanner(
    private var context: Context,
    private var activity: FragmentActivity,
    private var view: View
) {
    private val adView: AdView = AdView(context)

    companion object {
        private var AD_UNIT_ID = "ca-app-pub-7338795990329943/2672722922"

        fun newInstance(
            context: Context,
            activity: FragmentActivity,
            view: View,
            adUnitId: String
        ): AdMobAdaptiveBanner {
            AD_UNIT_ID = adUnitId
            return AdMobAdaptiveBanner(context, activity, view)
        }
    }

    fun getBanner() {
        Timber.d("id: $AD_UNIT_ID")
        adView.adUnitId = AD_UNIT_ID
        adView.adSize = adSize

        // Create an ad request. Check your logcat output for the hashed device ID to
        // get test ads on a physical device, e.g.,
        // "Use AdRequest.Builder.addTestDevice("ABCDE0123") to get test ads on this device."
        val adRequest = AdRequest.Builder().build()

        // Start loading the ad in the background.
        adView.loadAd(adRequest)
    }

    fun getAdView(): AdView = adView

    // Determine the screen width (less decorations) to use for the ad width.
    // If the ad hasn't been laid out, default to the full screen width.
    private val adSize: AdSize
        get() {
            val display = activity.windowManager.defaultDisplay
            val outMetrics = DisplayMetrics()
            display.getMetrics(outMetrics)

            val density = outMetrics.density

            var adWidthPixels = view.width.toFloat()
            if (adWidthPixels == 0f) {
                adWidthPixels = outMetrics.widthPixels.toFloat()
            }

            val adWidth = (adWidthPixels / density).toInt()
            return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(
                context,
                adWidth
            )
        }
}