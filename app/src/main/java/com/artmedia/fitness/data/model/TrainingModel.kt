package com.artmedia.fitness.data.model

import java.util.*

data class TrainingModel(
    val id: String,
    val allTime: Int,
    val categoryName: String,
    val firstItem: Boolean,
    val imageName: String,
    val premium: Boolean,
    val showSubTitle: Boolean,
    val subTitle: String,
    val title: String,
    val workoutList: List<HashMap<String, Objects>>,
    val workouts: Int
)