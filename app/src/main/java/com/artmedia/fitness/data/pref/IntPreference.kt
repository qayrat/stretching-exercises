package com.artmedia.fitness.data.pref

import android.content.SharedPreferences
import androidx.core.content.edit
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

class IntPreference(
    private val pref: SharedPreferences,
    private val key: String,
    private val defValue: Int
) : ReadWriteProperty<Any, Int> {
    override fun getValue(thisRef: Any, property: KProperty<*>): Int =
        pref.getInt(key, defValue) ?: 0


    override fun setValue(thisRef: Any, property: KProperty<*>, value: Int) {
        pref.edit { putInt(key, value) }
    }
}