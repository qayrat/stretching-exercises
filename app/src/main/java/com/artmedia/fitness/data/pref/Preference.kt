package com.artmedia.fitness.data.pref

import android.content.SharedPreferences

private const val TRAINING_ITEM_ID = "TRAINING_ITEM_ID"
private const val CREATE_REMINDER = "CREATE_REMINDER"
private const val IS_LOGIN = "IS_LOGIN"
private const val DAY1 = "MONDAY"
private const val DAY2 = "TUESDAY"
private const val DAY3 = "WEDNESDAY"
private const val DAY4 = "THURSDAY"
private const val DAY5 = "FRIDAY"
private const val DAY6 = "SATURDAY"
private const val DAY7 = "SUNDAY"
private const val SELECTED_LANGUAGE = "SELECTED_LANGUAGE"
private const val PREMIUM_MONTHLY = "PREMIUM_MONTHLY"
private const val PREMIUM_YEARLY = "PREMIUM_YEARLY"

class Preference(pref: SharedPreferences) : IPreference {
    override var trainingItemId: String by StringPreference(pref, TRAINING_ITEM_ID, "")
    override var createReminder: Boolean by BooleanPreference(pref, CREATE_REMINDER, false)
    override var isLogin: Boolean by BooleanPreference(pref, IS_LOGIN, false)
    override var monday: Int by IntPreference(pref, DAY1, 1)
    override var tuesday: Int by IntPreference(pref, DAY2, 2)
    override var wednesday: Int by IntPreference(pref, DAY3, 3)
    override var thursday: Int by IntPreference(pref, DAY4, 4)
    override var friday: Int by IntPreference(pref, DAY5, 5)
    override var saturday: Int by IntPreference(pref, DAY6, 6)
    override var sunday: Int by IntPreference(pref, DAY7, 7)
    override var selectedLanguage: String by StringPreference(pref, SELECTED_LANGUAGE, "en")
    override var premiumMonthly: Boolean by BooleanPreference(pref, PREMIUM_MONTHLY, false)
    override var premiumYearly: Boolean by BooleanPreference(pref, PREMIUM_YEARLY, false)
}