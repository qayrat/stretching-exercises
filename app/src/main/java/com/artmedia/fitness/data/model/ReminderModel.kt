package com.artmedia.fitness.data.model

class ReminderModel(
    val id: Int,
    val hour: Int,
    val alarm: Int,
    val monday: Int,
    val tuesday: Int,
    val wednesday: Int,
    val thursday: Int,
    val friday: Int,
    val saturday: Int,
    val sunday: Int,
    val minute: Int
)