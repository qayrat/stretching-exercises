package com.artmedia.fitness.data.database;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.artmedia.fitness.data.model.ReminderModel;
import com.artmedia.fitness.libs.DataBaseHelper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class DBHelper {
    private static final String TAG = "TAG";
    private final Context mContext;
    private SQLiteDatabase mDb;
    private DataBaseHelper helper;
    @SuppressLint("StaticFieldLeak")
    private static DBHelper database;

    private DBHelper(Context mContext) {
        this.mContext = mContext;
        helper = new DataBaseHelper(mContext);
    }

    public static DBHelper getDatabase() {
        return database;
    }

    public static DBHelper init(Context context) {
        if (database == null) {
            database = new DBHelper(context);
        }
        return database;
    }

    public DBHelper createDataBase() {
        try {
            helper.createDataBase();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return this;
    }

    public DBHelper open() throws SQLException {
        try {
            helper.openDataBase();
            helper.close();
            mDb = helper.getReadableDatabase();
        } catch (SQLException mSQLException) {
            Log.e(TAG, "open >>" + mSQLException.toString());
            throw mSQLException;
        }
        return this;
    }

    public void close() {
        helper.close();
    }

    //Reminder
    public ArrayList<ReminderModel> getReminder() {
        ArrayList<ReminderModel> data = new ArrayList<>();
        Cursor cursor = mDb.rawQuery("select * from reminder", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            data.add(new ReminderModel(
                    cursor.getInt(cursor.getColumnIndex("_id")),
                    cursor.getInt(cursor.getColumnIndex("hour")),
                    cursor.getInt(cursor.getColumnIndex("alarm")),
                    cursor.getInt(cursor.getColumnIndex("monday")),
                    cursor.getInt(cursor.getColumnIndex("tuesday")),
                    cursor.getInt(cursor.getColumnIndex("wednesday")),
                    cursor.getInt(cursor.getColumnIndex("thursday")),
                    cursor.getInt(cursor.getColumnIndex("friday")),
                    cursor.getInt(cursor.getColumnIndex("saturday")),
                    cursor.getInt(cursor.getColumnIndex("sunday")),
                    cursor.getInt(cursor.getColumnIndex("minute"))
            ));
            cursor.moveToNext();
        }
        cursor.close();
        return data;
    }

    public void insertReminder(ReminderModel reminder) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("hour", reminder.getHour());
        contentValues.put("alarm", reminder.getAlarm());
        contentValues.put("monday", reminder.getMonday());
        contentValues.put("tuesday", reminder.getTuesday());
        contentValues.put("wednesday", reminder.getWednesday());
        contentValues.put("thursday", reminder.getThursday());
        contentValues.put("friday", reminder.getFriday());
        contentValues.put("saturday", reminder.getSaturday());
        contentValues.put("sunday", reminder.getSunday());
        contentValues.put("minute", reminder.getMinute());
        mDb.insert("reminder", null, contentValues);
    }

    public void deleteReminder(int id) {
        mDb.delete("reminder", "_id=?", new String[]{Integer.toString(id)});
    }

    public void updateReminderTime(int id, int hour, int minute) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("hour", hour);
        contentValues.put("minute", minute);
        mDb.update("reminder", contentValues, "_id = ?", new String[]{String.valueOf(id)});
    }

    public void updateReminderAlarm(int id, int alarm) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("alarm", alarm);
        mDb.update("reminder", contentValues, "_id = ?", new String[]{String.valueOf(id)});
    }

    public void updateReminderWeek(int id, int[] list) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("monday", list[0]);
        contentValues.put("tuesday", list[1]);
        contentValues.put("wednesday", list[2]);
        contentValues.put("thursday", list[3]);
        contentValues.put("friday", list[4]);
        contentValues.put("saturday", list[5]);
        contentValues.put("sunday", list[6]);
        mDb.update("reminder", contentValues, "_id = ?", new String[]{String.valueOf(id)});
    }
}
