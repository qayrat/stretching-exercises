package com.artmedia.fitness.data.pref

interface IPreference {
    var trainingItemId: String
    var createReminder: Boolean
    var isLogin: Boolean
    var monday: Int
    var tuesday: Int
    var wednesday: Int
    var thursday: Int
    var friday: Int
    var saturday: Int
    var sunday: Int
    var selectedLanguage: String
    var premiumMonthly: Boolean
    var premiumYearly: Boolean
}