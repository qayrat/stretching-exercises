package com.artmedia.fitness

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import android.util.Log
import androidx.preference.PreferenceManager
import com.artmedia.fitness.data.database.DBHelper
import com.artmedia.fitness.data.pref.IPreference
import com.artmedia.fitness.data.pref.Preference
import com.artmedia.fitness.libs.LocaleHelper
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.LoadAdError
import com.google.android.gms.ads.interstitial.InterstitialAd
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback
import com.kulbaev.fitness.BuildConfig
import timber.log.Timber

class App : Application() {
    companion object {
        @SuppressLint("StaticFieldLeak")
        var context: Context? = null
        var mInterstitialAd: InterstitialAd? = null
        val pref: IPreference by lazy(LazyThreadSafetyMode.NONE) {
            Preference(PreferenceManager.getDefaultSharedPreferences(context!!))
        }
    }

    override fun onCreate() {
        super.onCreate()
        context = this
        LocaleHelper.onAttach(applicationContext, pref.selectedLanguage)
        DBHelper.init(this)
        DBHelper.getDatabase().createDataBase().open()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
        if (!(pref.premiumMonthly || pref.premiumYearly)) {
            loadAds()
        }
//        pref.selectedLanguage = "en"
//        LocaleHelper.onAttach(this, "en")
    }

    private fun loadAds() {
        val adRequest = AdRequest.Builder().build()
        InterstitialAd.load(
            this,
            "ca-app-pub-7338795990329943/1209228325",
            adRequest,
            object : InterstitialAdLoadCallback() {
                override fun onAdFailedToLoad(adError: LoadAdError) {
                    mInterstitialAd = null
                    Log.d("onAdFailedToLoadApp","${adError.code}")
                }

                override fun onAdLoaded(interstitialAd: InterstitialAd) {
                    mInterstitialAd = interstitialAd
                }
            })
    }

    override fun onTerminate() {
        super.onTerminate()
        mInterstitialAd = null
        DBHelper.getDatabase().close()
    }
}