package com.artmedia.fitness.receiver

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.media.AudioAttributes
import android.media.RingtoneManager
import android.os.Build
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.content.ContextCompat
import androidx.preference.PreferenceManager
import com.artmedia.fitness.data.database.DBHelper
import com.artmedia.fitness.data.model.ReminderModel
import com.artmedia.fitness.data.pref.IPreference
import com.artmedia.fitness.data.pref.Preference
import com.artmedia.fitness.libs.CHANNEL_ID
import com.artmedia.fitness.libs.NOTIFICATION_ID
import com.artmedia.fitness.ui.main.MainActivity
import com.kulbaev.fitness.R
import timber.log.Timber
import java.util.*

class AlarmReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        val calendar = Calendar.getInstance()
        val dayOfWeek: Int = calendar.get(Calendar.DAY_OF_WEEK)
        DBHelper.init(context)
        DBHelper.getDatabase().createDataBase().open()
        val reminder: ReminderModel = DBHelper.getDatabase().reminder[0]
        val checkedItemsReminder = intArrayOf(
            reminder.monday,
            reminder.tuesday,
            reminder.wednesday,
            reminder.thursday,
            reminder.friday,
            reminder.saturday,
            reminder.sunday
        )
        Timber.d("${reminder.thursday}")
        when (dayOfWeek) {
            Calendar.MONDAY -> {
                if (checkedItemsReminder[0] == 1) buildNotification(context)
            }
            Calendar.TUESDAY -> {
                if (checkedItemsReminder[1] == 1) buildNotification(context)
            }
            Calendar.WEDNESDAY -> {
                if (checkedItemsReminder[2] == 1) buildNotification(context)
            }
            Calendar.THURSDAY -> {
                if (checkedItemsReminder[3] == 1) buildNotification(context)
            }
            Calendar.FRIDAY -> {
                if (checkedItemsReminder[4] == 1) buildNotification(context)
            }
            Calendar.SATURDAY -> {
                if (checkedItemsReminder[5] == 1) buildNotification(context)
            }
            Calendar.SUNDAY -> {
                if (checkedItemsReminder[6] == 1) buildNotification(context)
            }
        }
        if (intent.action == "android.intent.action.BOOT_COMPLETED") {
            // Set the alarm here.
        }
//        val timeInMillis = intent.getLongExtra(Constants.EXTRA_EXACT_ALARM_TIME, 0L)
//        when (intent.action) {
//            Constants.ACTION_SET_REPETITIVE_ALARM -> {
//                val calendar = Calendar.getInstance().apply {
//                    this.timeInMillis = timeInMillis + TimeUnit.DAYS.toMillis(7)
//                }
////                AlarmService(context).setRepetitiveAlarm(timeInMillis)
//                Timber.d("ACTION_SET_REPETITIVE_ALARM")
//            }
//        }
    }

    private fun buildNotification(context: Context) {
        val pref: IPreference by lazy(LazyThreadSafetyMode.NONE) {
            Preference(PreferenceManager.getDefaultSharedPreferences(context))
        }
        var contentTitle = "Stretching exercises"
        var contentText = "It's time to train. Let's start!"
        when (pref.selectedLanguage) {
            context.getString(R.string.russian_tag) -> {
                contentTitle = "Упражнения для растяжки"
                contentText = "Пора заниматься. Начнем!"
            }
            context.getString(R.string.english_tag) -> {
                contentTitle = "Stretching exercises"
                contentText = "It's time to train. Let's start!"
            }
        }
        val intent = Intent(context, MainActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }
        val pendingIntent: PendingIntent = PendingIntent.getActivity(context, 0, intent, 0)
        createNotificationChannel(context, contentTitle, contentText)
        val alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val builder = NotificationCompat.Builder(context, CHANNEL_ID)
            .setSmallIcon(R.drawable.notification_icon)
            .setColor(ContextCompat.getColor(context, R.color.green))
            .setContentTitle(contentTitle)
            .setContentText(contentText)
            .setPriority(NotificationCompat.PRIORITY_HIGH)
            .setVibrate(longArrayOf(500, 500))
            .setSound(alarmSound)
            .setContentIntent(pendingIntent)
            .setAutoCancel(true)
            .setCategory(NotificationCompat.CATEGORY_ALARM)
        with(NotificationManagerCompat.from(context)) {
            notify(NOTIFICATION_ID, builder.build())
        }
    }

    private fun createNotificationChannel(context: Context, title: String, text: String) {
        val alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val importance = NotificationManager.IMPORTANCE_HIGH
            val channel = NotificationChannel(CHANNEL_ID, title, importance).apply {
                description = text
                enableVibration(true)
                vibrationPattern = longArrayOf(500, 500)
                val audioAttributes = AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                    .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                    .build()
                setSound(alarmSound, audioAttributes)
            }
            val notificationManager: NotificationManager =
                context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }
}