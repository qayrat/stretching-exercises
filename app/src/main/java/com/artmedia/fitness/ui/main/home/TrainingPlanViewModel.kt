package com.artmedia.fitness.ui.main.home

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.artmedia.fitness.App
import com.artmedia.fitness.data.model.TrainingModel
import com.google.firebase.firestore.Source
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import timber.log.Timber
import java.util.*
import kotlin.collections.ArrayList

class TrainingPlanViewModel : ViewModel() {
    private val _list = MutableLiveData<ArrayList<TrainingModel>>()
    val list: LiveData<ArrayList<TrainingModel>> = _list
    private final var TAG = "TrainingPlanViewModel"

    fun getTrainingPlans() {
        val data = ArrayList<TrainingModel>()

        //create FireStore
        val db = Firebase.firestore
        val source = Source.CACHE
        var collection = ""
        when (App.pref.selectedLanguage) {
            "en" -> {
                collection = "en"
            }
            "ru" -> {
                collection = "data"
            }
        }
        Timber.d(collection)
        db.collection(collection)
            .get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    val workoutList: List<HashMap<String, Objects>> =
                        document["workoutList"] as List<HashMap<String, Objects>>
                    val item = TrainingModel(
                        document.id,
                        document["allTime"].toString().toInt(),
                        document["categoryName"].toString(),
                        document["firstItem"].toString().toBoolean(),
                        document["imageName"].toString(),
                        document["premium"].toString().toBoolean(),
                        document["showSubTitle"].toString().toBoolean(),
                        document["subTitle"].toString(),
                        document["title"].toString(),
                        workoutList,
                        document["workouts"].toString().toInt(),
                    )
                    data.add(item)
                    Log.d(TAG, "${document.id} => ${document.data}")
                }
                data.removeAt(data.size - 1)
                data.removeAt(data.size - 1)
                data.removeAt(data.size - 1)
                _list.value = data

//                if (collection == "data") {
//                    db.collection("en").document("27")
//                        .set(data[0])
//                        .addOnSuccessListener {
//                            Log.w(TAG, "Success setting documents. 1")
//                        }
//                        .addOnFailureListener {
//                            Log.w(TAG, "Error setting documents.", it)
//                        }
//                }
            }
            .addOnFailureListener { exception ->
                Log.w(TAG, "Error getting documents.", exception)
            }
    }
}