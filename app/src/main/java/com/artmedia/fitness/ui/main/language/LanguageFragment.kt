package com.artmedia.fitness.ui.main.language

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.TextView
import androidx.core.view.get
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.artmedia.fitness.App
import com.artmedia.fitness.libs.LocaleHelper
import com.kulbaev.fitness.R
import kotlinx.android.synthetic.main.fragment_language.*
import kotlinx.android.synthetic.main.fragment_language.view.*
import timber.log.Timber

class LanguageFragment : DialogFragment() {
    private lateinit var viewModel: LanguageViewModel
    private var radioButtonCheckedId = 0

    override fun onStart() {
        super.onStart()
        val d = dialog
        if (d != null) {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT
            d.window!!.setLayout(width, height)
        }
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.fragment_language, container, false)
        when (App.pref.selectedLanguage) {
            "en" -> {
                root.radio_btn1.isChecked = true
                radioButtonCheckedId = root.radio_btn1.id
            }
            "ru" -> {
                root.radio_btn2.isChecked = true
                radioButtonCheckedId = root.radio_btn2.id
            }
        }
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ok_button.setOnClickListener {
            if (radioButtonCheckedId != radio_group.checkedRadioButtonId) {
                when (radio_group.checkedRadioButtonId) {
                    radio_btn1.id -> {
                        LocaleHelper.setLocale(requireContext(), radio_btn1.tag.toString())
                        App.pref.selectedLanguage = radio_btn1.tag.toString()
                        dismiss()
                        requireActivity().recreate()
                    }
                    radio_btn2.id -> {
                        LocaleHelper.setLocale(requireContext(), radio_btn2.tag.toString())
                        App.pref.selectedLanguage = radio_btn2.tag.toString()
                        dismiss()
                        requireActivity().recreate()
                    }
                }
            } else {
                dismiss()
            }
        }
        cancel_button.setOnClickListener {
            dismiss()
        }
    }

//    override fun onAttach(context: Context) {
//        super.onAttach(LocaleHelper.onAttach(context))
//    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        return dialog
    }
}