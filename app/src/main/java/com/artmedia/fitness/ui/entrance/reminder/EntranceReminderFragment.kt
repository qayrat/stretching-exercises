package com.artmedia.fitness.ui.entrance.reminder

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.artmedia.fitness.App
import com.artmedia.fitness.data.database.DBHelper
import com.artmedia.fitness.data.model.ReminderModel
import com.artmedia.fitness.receiver.AlarmReceiver
import com.artmedia.fitness.service.AlarmService
import com.kulbaev.fitness.R
import kotlinx.android.synthetic.main.entrance_reminder_fragment.*
import timber.log.Timber
import java.util.*

class EntranceReminderFragment : Fragment(R.layout.entrance_reminder_fragment) {
    private lateinit var alarmService: AlarmService
    private var hour = 9
    private var minute = 0
    private val navController by lazy(LazyThreadSafetyMode.NONE) {
        Navigation.findNavController(requireActivity(), R.id.entrance_nav_fragment)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        alarmService = AlarmService(App.context!!)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        time_picker.setIs24HourView(true)
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            time_picker.hour = 9
            time_picker.minute = 0
        } else {
            time_picker.currentHour = 9
            time_picker.currentMinute = 0
        }

        btn_finish.setOnClickListener {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                hour = time_picker.hour
                minute = time_picker.minute
            } else {
                hour = time_picker.currentHour
                minute = time_picker.currentMinute
            }
            App.pref.isLogin = true
            addReminder()
        }
    }

    private fun addReminder() {
        Timber.d("$hour : $minute")
        val data = ReminderModel(
            1,
            hour,
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            1,
            minute
        )
        DBHelper.getDatabase().insertReminder(data)

        val calendar = Calendar.getInstance().apply {
            timeInMillis = System.currentTimeMillis()
            set(Calendar.HOUR_OF_DAY, hour)
            set(Calendar.MINUTE, minute)
            set(Calendar.SECOND, 0)
        }
        val alarmIntent = Intent(requireContext(), AlarmReceiver::class.java).let {
            PendingIntent.getBroadcast(requireContext(), 1, it, 0)
        }
        val am = requireContext().getSystemService(Context.ALARM_SERVICE) as AlarmManager
        am.setInexactRepeating(
            AlarmManager.RTC_WAKEUP,
            calendar.timeInMillis,
            AlarmManager.INTERVAL_DAY,
            alarmIntent
        )
//        setAlarm(Calendar.MONDAY, hour, minute, 1)
//        setAlarm(Calendar.TUESDAY, hour, minute, 2)
//        setAlarm(Calendar.WEDNESDAY, hour, minute, 3)
//        setAlarm(Calendar.THURSDAY, hour, minute, 4)
//        setAlarm(Calendar.FRIDAY, hour, minute, 5)
//        setAlarm(Calendar.SATURDAY, hour, minute, 6)
//        setAlarm(Calendar.SUNDAY, hour, minute, 7)
        App.pref.monday = 1
        App.pref.tuesday = 2
        App.pref.wednesday = 3
        App.pref.thursday = 4
        App.pref.friday = 5
        App.pref.saturday = 6
        App.pref.sunday = 7
        navController.navigate(R.id.action_reminderFragment_to_mainActivity)
        requireActivity().finish()
    }

    private fun setAlarm(dayOfWeek: Int, hourOfDay: Int, minute: Int, requestCode: Int) {
        val calendar: Calendar = Calendar.getInstance().apply {
            timeInMillis = System.currentTimeMillis()
            set(Calendar.HOUR_OF_DAY, hourOfDay)
            set(Calendar.MINUTE, minute)
            set(Calendar.SECOND, 0)
        }

//        if (calendar.timeInMillis < System.currentTimeMillis()) {
//            calendar.add(Calendar.DAY_OF_YEAR, 7)
//        }
        alarmService.setRepetitiveAlarm(calendar.timeInMillis, requestCode)
    }
}