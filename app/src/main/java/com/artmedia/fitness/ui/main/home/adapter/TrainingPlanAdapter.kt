package com.artmedia.fitness.ui.main.home.adapter

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.artmedia.fitness.App
import com.artmedia.fitness.data.model.TrainingModel
import com.artmedia.fitness.libs.CacheImageManager
import com.bumptech.glide.Glide
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import com.kulbaev.fitness.R
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.fragment_home_item.*
import timber.log.Timber
import java.io.File

class TrainingPlanAdapter(
    private val fragment: Fragment,
    private val data: ArrayList<TrainingModel>
) : RecyclerView.Adapter<TrainingPlanAdapter.ViewHolder>() {
    private val layoutInflater = LayoutInflater.from(fragment.requireContext())
    private val listener = fragment as OnItemClickListener
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val view = layoutInflater.inflate(R.layout.fragment_home_item, parent, false)
        return ViewHolder(view, listener, fragment)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bindData(data[position])

    override fun getItemCount(): Int = data.size

    class ViewHolder(
        itemView: View,
        listener: OnItemClickListener,
        private val fragment: Fragment
    ) : RecyclerView.ViewHolder(itemView), LayoutContainer {
        override val containerView: View
            get() = itemView

        init {
            item_click.setOnClickListener {
                listener.onClick(data)
            }
        }

        private lateinit var data: TrainingModel
        private val storageRef = Firebase.storage.reference
        private final var TAG = "TrainingViewHolder"

        fun bindData(item: TrainingModel) {
            val time = "${item.allTime} ${fragment.resources.getString(R.string.minutes)}"
            val workouts = "${item.workouts} ${fragment.resources.getString(R.string.workouts)}"
            data = item
            tv_title.text = item.title
            tv_time.text = time
            tv_workout.text = workouts
            if (item.firstItem) {
                tv_category_name.visibility = View.VISIBLE
                tv_category_name.text = item.categoryName
            } else {
                tv_category_name.visibility = View.GONE
            }

            if (item.showSubTitle) {
                iv_time_icon.visibility = View.INVISIBLE
                tv_time.visibility = View.INVISIBLE
                iv_workout_icon.visibility = View.INVISIBLE
                tv_workout.visibility = View.INVISIBLE
                tv_sub_title.visibility = View.VISIBLE
                tv_sub_title.text = item.subTitle
            } else {
                iv_time_icon.visibility = View.VISIBLE
                tv_time.visibility = View.VISIBLE
                iv_workout_icon.visibility = View.VISIBLE
                tv_workout.visibility = View.VISIBLE
                tv_sub_title.visibility = View.GONE
            }

            if (item.premium) {
                iv_premium.visibility = View.VISIBLE
            } else {
                iv_premium.visibility = View.GONE
            }
            val pathUri = "file:///android_asset/homeImages/${item.imageName}"
            Glide.with(App.context!!)
                .load(pathUri)
                .error(R.drawable.default_image)
                .into(iv_image)
        }

        private fun downloadImage(filename: String) {
            val islandRef = storageRef.child("images/trainingPlans/$filename")
            val localFile = File.createTempFile("images", "jpg")
            islandRef.getFile(localFile).addOnSuccessListener {
                val bitmap: Bitmap = BitmapFactory.decodeFile(localFile.absolutePath)
                CacheImageManager.putImage(App.context!!, filename, localFile)
                Timber.d(localFile.absolutePath)
                Glide.with(App.context!!)
                    .load(bitmap)
                    .into(iv_image)
                Log.d(TAG, "success download image")
            }
                .addOnFailureListener { exception ->
                    Log.w(TAG, "Error.", exception)
                }
            //            islandRef.downloadUrl.addOnSuccessListener {
//                Log.d(TAG, "success download image")
//                Glide.with(fragment)
//                    .load(it)
//                    .thumbnail(0.1f)
//                    .error(R.drawable.default_image)
//                    .transition(DrawableTransitionOptions.withCrossFade())
//                    .diskCacheStrategy(DiskCacheStrategy.DATA)
//                    .into(iv_image)
//            }
//                .addOnFailureListener { exception ->
//                    Log.w(TAG, "Error.", exception)
//                }
        }
    }

    interface OnItemClickListener {
        fun onClick(d: TrainingModel)
    }
}