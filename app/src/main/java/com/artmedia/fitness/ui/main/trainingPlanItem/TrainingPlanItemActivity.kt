package com.artmedia.fitness.ui.main.trainingPlanItem

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.artmedia.fitness.App
import com.artmedia.fitness.libs.LocaleHelper
import com.artmedia.fitness.ui.main.getPremium.PremiumActivity
import com.bumptech.glide.Glide
import com.google.android.material.appbar.AppBarLayout
import com.kulbaev.fitness.R
import com.kulbaev.fitness.databinding.ActivityTrainingPlanItemBinding
import kotlinx.android.synthetic.main.activity_training_plan_item.*
import kotlinx.android.synthetic.main.fragment_premium_subscription_dialog.*

class TrainingPlanItemActivity : AppCompatActivity() {
    private lateinit var binding: ActivityTrainingPlanItemBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTrainingPlanItemBinding.inflate(layoutInflater)
        val view = binding.root
        val premium = intent.getBooleanExtra("trainingPlanPremium", true)
        if (premium) {
            if (!(App.pref.premiumMonthly || App.pref.premiumYearly)) {
                setContentView(R.layout.fragment_premium_subscription_dialog)
                setPremiumContentView(savedInstanceState)
            } else {
                setContentView(view)
                setMainContentView(savedInstanceState)
            }
        } else {
            setContentView(view)
            setMainContentView(savedInstanceState)
        }
    }

    private fun setPremiumContentView(savedInstanceState: Bundle?) {
        go_premium.setOnClickListener {
            val intent = Intent(this, PremiumActivity::class.java)
            startActivity(intent)
        }

        back.setOnClickListener {
            finish()
        }
    }

    override fun onRestart() {
        super.onRestart()
        recreate()
    }

    private fun setMainContentView(savedInstanceState: Bundle?) {
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        val planTitle = intent.getStringExtra("trainingPlanTitle")
        val imageName = intent.getStringExtra("trainingPlanImageName")

        //create fragment
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.container_training_plan, TrainingPlanItemFragment.newInstance())
                .commitNow()
        }

        setData(imageName!!, planTitle!!)
//        if (premium) {
//            val premiumDialog = PremiumSubscriptionDialog()
//            premiumDialog.show(supportFragmentManager, "premiumDialog")
//            premiumDialog.listener = object : PremiumSubscriptionDialog.OnDismissCancelListener {
//                override fun onDismiss(dialog: DialogInterface) {
//                    finish()
//                }
//            }
//        }

        //hide title expanded
        var isShow = true
        var scrollRange = -1
        binding.appBar.addOnOffsetChangedListener(AppBarLayout.OnOffsetChangedListener { appBarLayout, verticalOffset ->
            if (scrollRange == -1) {
                scrollRange = appBarLayout?.totalScrollRange!!
            }
            if (scrollRange + verticalOffset == 0) {
                binding.toolbarLayout.title = planTitle
                isShow = true
            } else if (isShow) {
                binding.toolbarLayout.title =
                    " " //careful there should a space between double quote otherwise it wont work
                isShow = false
            }
        })
    }

    private fun setData(imageName: String, title: String) {
        binding.tvTitle.text = title
        binding.tvSubTitle.text = title
        val pathUri = "file:///android_asset/homeImages/${imageName}"
        Glide.with(App.context!!)
            .load(pathUri)
            .error(R.drawable.default_image)
            .into(target_image_view)
    }

    override fun onSupportNavigateUp(): Boolean {
        App.pref.trainingItemId = ""
        finish()
        return true
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(LocaleHelper.onAttach(newBase))
    }
}