package com.artmedia.fitness.ui.main.workoutFinish

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.artmedia.fitness.App
import com.artmedia.fitness.data.database.DBHelper
import com.artmedia.fitness.libs.LocaleHelper
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.LoadAdError
import com.google.android.gms.ads.MobileAds
import com.google.android.gms.ads.interstitial.InterstitialAd
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback
import com.google.android.material.appbar.AppBarLayout
import com.kulbaev.fitness.databinding.ActivityWorkoutFinishBinding
import kotlinx.android.synthetic.main.content_scrolling_workout_finish.*
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*

class WorkoutFinishActivity : AppCompatActivity() {
    private lateinit var binding: ActivityWorkoutFinishBinding
    private var mInterstitialAd: InterstitialAd? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityWorkoutFinishBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)

        //google AdMobs
        MobileAds.initialize(this)
        Timber.d("${(App.pref.premiumMonthly || App.pref.premiumYearly)}")
        if (!(App.pref.premiumMonthly || App.pref.premiumYearly)) {
            Timber.d("${(App.pref.premiumMonthly || App.pref.premiumYearly)}")
            loadAd()
        }

        val data = DBHelper.getDatabase().reminder[0]
        val timeReminder = String.format("%02d:%02d", data.hour, data.minute)
        val allTime = intent.getIntExtra("workoutsAllTime", 0)
        val workoutCount = intent.getIntExtra("workoutCount", 0)
        Timber.d("$workoutCount")
        binding.tvWorkout.text = workoutCount.toString()
        binding.tvDuration.text = convertTime((allTime.toLong() * 1000))
        reminder_time.text = timeReminder

        binding.doItAgain.setOnClickListener {
            finish()
        }
        tv_following.setOnClickListener {
            finish()
        }

        //hide title expanded
        var isShow = true
        var scrollRange = -1
        binding.appBar.addOnOffsetChangedListener(AppBarLayout.OnOffsetChangedListener { appBarLayout, verticalOffset ->
            if (scrollRange == -1) {
                scrollRange = appBarLayout?.totalScrollRange!!
            }
            if (scrollRange + verticalOffset == 0) {
                binding.toolbarLayout.title = "Выполнен"
                isShow = true
            } else if (isShow) {
                binding.toolbarLayout.title =
                    " " //careful there should a space between double quote otherwise it wont work
                isShow = false
            }
        })
    }

    private fun loadAd() {
        val adRequest = AdRequest.Builder().build()
        InterstitialAd.load(
            this,
            "ca-app-pub-7338795990329943/1209228325",
            adRequest,
            object : InterstitialAdLoadCallback() {
                override fun onAdFailedToLoad(adError: LoadAdError) {
                    Timber.d(adError.message)
                    mInterstitialAd = null
                }

                override fun onAdLoaded(interstitialAd: InterstitialAd) {
                    Timber.d("Ad was loaded.")
                    mInterstitialAd = interstitialAd
                    mInterstitialAd?.show(this@WorkoutFinishActivity)
                }
            })
    }

    @SuppressLint("SimpleDateFormat")
    private fun convertTime(time: Long): String {
        return SimpleDateFormat("mm:ss").format(Date(time))
    }

    override fun onSupportNavigateUp(): Boolean {
        finish()
        return true
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(LocaleHelper.onAttach(newBase))
    }
}