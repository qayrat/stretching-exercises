package com.artmedia.fitness.ui.entrance.splash

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.artmedia.fitness.App
import com.kulbaev.fitness.R

class SplashFragment : Fragment(R.layout.splash_fragment) {
    private var isLogin: Boolean? = null
    private val navController by lazy(LazyThreadSafetyMode.NONE) {
        Navigation.findNavController(requireActivity(), R.id.entrance_nav_fragment)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        isLogin = App.pref.isLogin
    }

    override fun onResume() {
        super.onResume()
        val handler = Handler(Looper.getMainLooper())
        handler.postDelayed({
            run {
                if (isLogin!!) {
                    navController.navigate(R.id.action_splashFragment_to_mainActivity)
                    requireActivity().finish()
                } else {
                    navController.navigate(R.id.action_splashFragment_to_reminderFragment)
                }
            }
        }, 2000)
    }
}