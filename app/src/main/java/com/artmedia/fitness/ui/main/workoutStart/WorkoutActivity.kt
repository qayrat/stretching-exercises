package com.artmedia.fitness.ui.main.workoutStart

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.media.AudioAttributes
import android.media.AudioManager
import android.media.SoundPool
import android.os.*
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.artmedia.fitness.App
import com.artmedia.fitness.data.model.TrainingModel
import com.artmedia.fitness.libs.LocaleHelper
import com.artmedia.fitness.ui.main.workoutFinish.WorkoutFinishActivity
import com.artmedia.fitness.ui.main.workoutStart.dialog.ExitDialogFragment
import com.artmedia.fitness.ui.main.workoutStart.dialog.InfoDialogFragment
import com.artmedia.fitness.ui.main.workoutStart.dialog.RelaxationDialogFragment
import com.bumptech.glide.Glide
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import com.kulbaev.fitness.R
import com.kulbaev.fitness.databinding.ActivityWorkoutBinding
import timber.log.Timber
import java.util.*

class WorkoutActivity : AppCompatActivity() {
    private lateinit var binding: ActivityWorkoutBinding
    private lateinit var viewModel: WorkoutViewModel
    private var trainingModel: TrainingModel? = null
    private val storageRef = Firebase.storage.reference
    private lateinit var handler: Handler
    private var mCountDownTimer: CountDownTimer? = null
    private var workoutCount = 0
    private var workoutPosition = 0
    private var plusLength = 0f
    private var proLength = 0f
    private var workoutTime = 0
    private var startTimeFinish = false
    private var countDownTimer = true
    private var planId = ""

    //top ProgressBar
    private var topProgress = 0
    private var topProgressPlusLength = 0f

    //SoundPool
    private lateinit var soundPool: SoundPool
    private var soundStart = 0
    private var soundFinish = 0
    private var soundFinishWorkout = 0

    //Workout Stopwatch
    private var seconds = 0
    private var running: Boolean = true
    private var wasRunning: Boolean = false
    private lateinit var handlerAllTime: Handler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityWorkoutBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        if (savedInstanceState != null) {
            seconds = savedInstanceState.getInt("seconds")
            running = savedInstanceState.getBoolean("running")
            wasRunning = savedInstanceState.getBoolean("wasRunning")
        }
        planId = App.pref.trainingItemId
        handler = Handler(Looper.getMainLooper())
        viewModel = ViewModelProvider(this).get(WorkoutViewModel::class.java)
        viewModel.getTrainingItem(planId)
        viewModel.list.observe(this, Observer {
            startTime(it)
            trainingModel = it
        })

        //allTime handler start
        handlerAllTime = Handler(Looper.getMainLooper())
        handlerAllTime.post(runnableAllTime)

        //create soundPool
        createSoundPool()
        soundStart = soundPool.load(this, R.raw.start_sound, 1)
        soundFinish = soundPool.load(this, R.raw.finish_sound, 1)
        soundFinishWorkout = soundPool.load(this, R.raw.finish_workout_sound, 1)

        binding.ivPause.setOnClickListener {
            openInfoDialog(trainingModel!!.workoutList[workoutPosition])
            handler.removeCallbacks(runnable)
        }
        binding.ivNext.setOnClickListener {
            if ((workoutPosition + 1) == workoutCount) {
                //Workouts finish
                running = false
                handler.removeCallbacks(runnableAllTime)
                handler.removeCallbacks(runnable)
                openWorkoutFinish()
            } else {
                if (!countDownTimer) {
                    handler.removeCallbacks(runnable)
                    workoutPosition++
                    openRelaxationDialog(trainingModel!!, workoutPosition)
                } else {
                    mCountDownTimer?.onFinish()
                }
            }
        }
        binding.ivPrevious.setOnClickListener {
            handler.removeCallbacks(runnable)
            workoutPosition -= 1
            openRelaxationDialog(trainingModel!!, workoutPosition)
        }
        binding.ivHelp.setOnClickListener {
            openInfoDialog(trainingModel!!.workoutList[workoutPosition])
            handler.removeCallbacks(runnable)
        }
        binding.ivClose.setOnClickListener {
            openExitDialog()
        }
    }

    private fun openWorkoutFinish() {
        val intent = Intent(this, WorkoutFinishActivity::class.java)
        intent.putExtra("workoutsAllTime", seconds)
        intent.putExtra("workoutCount", workoutCount)
        startActivity(intent)
        finish()
    }

    private fun createSoundPool() {
        soundPool = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val audioAttributes: AudioAttributes = AudioAttributes
                .Builder()
                .setUsage(AudioAttributes.USAGE_ASSISTANCE_SONIFICATION)
                .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                .build()
            SoundPool
                .Builder()
                .setMaxStreams(2)
                .setAudioAttributes(audioAttributes)
                .build()
        } else {
            SoundPool(2, AudioManager.STREAM_MUSIC, 0)
        }
    }

    private var runnableAllTime: Runnable = object : Runnable {
        override fun run() {
            if (running) {
                seconds++
                Timber.d("Seconds: $seconds")
                handlerAllTime.postDelayed(this, 1000)
            }
        }
    }

    private var runnable: Runnable = object : Runnable {
        override fun run() {
            proLength = binding.bottomProgressBar.progress + (plusLength * 10)
            binding.bottomProgressBar.progress = proLength.toInt()
            if (workoutTime == 1) {
                proLength += 100
            }
            workoutTime -= 1
            binding.tvTime.text = workoutTime.toString()
            if (proLength < 1000) {
                handler.postDelayed(this, 1000)
            } else {
                workoutPosition++
                if (workoutPosition <= (workoutCount - 1)) {
                    bindItemData(trainingModel!!, workoutPosition)
                    openRelaxationDialog(trainingModel!!, workoutPosition)
                } else {
                    //Workouts finish
                    running = false
                    handler.removeCallbacks(runnableAllTime)
                    openWorkoutFinish()
                }
            }
        }
    }

    private fun startTime(it: TrainingModel) {
        workoutCount = it.workoutList.size
        val count: Float = workoutCount.toFloat()
        topProgressPlusLength = (1000 / count)
        bindItemData(it, workoutPosition)
        var time = 0
        binding.startProgressBar.progress = time
        mCountDownTimer = object : CountDownTimer(15000, 1000) {
            override fun onTick(p0: Long) {
                binding.tvStartTime.text = (p0 / 1000).toString()
                Timber.d("Tick of progress: $time$p0")
                time++
                binding.startProgressBar.setProgressCompat((time * 100 / (15000 / 1000)), true)
            }

            override fun onFinish() {
                time++
                binding.startProgressBar.setProgressCompat(100, true)
                binding.startTimeView.visibility = View.GONE
                binding.tvReady.visibility = View.GONE
                binding.ivHelp.visibility = View.VISIBLE
                binding.tvTime.visibility = View.VISIBLE
                binding.ivPause.visibility = View.VISIBLE
                binding.bottomProgressBar.visibility = View.VISIBLE
                startTimeFinish = true
                countDownTimer = false
                startWorkouts(it, workoutPosition)
                mCountDownTimer?.cancel()
            }
        }
        mCountDownTimer?.start()
    }

    private fun bindItemData(it: TrainingModel, position: Int) {
        binding.tvTitle.text = it.workoutList[position]["title"].toString()
        val pathUri =
            "file:///android_asset/workouts/${it.workoutList[position]["imageName"].toString()}"
        Glide.with(this)
            .load(pathUri)
            .error(R.drawable.default_image)
            .into(binding.ivImage)
//        val filename = it.workoutList[position]["imageName"].toString()
//        val islandRef = storageRef.child("images/workouts/$filename")
//        islandRef.downloadUrl.addOnSuccessListener {
//            Glide.with(this)
//                .load(it)
//                .thumbnail(0.1f)
//                .transition(DrawableTransitionOptions.withCrossFade())
//                .into(binding.ivImage)
//        }
//            .addOnFailureListener { exception ->
//                Timber.w(exception, "Error.")
//            }
    }

    private fun startWorkouts(data: TrainingModel, position: Int) {
        soundPool.play(soundStart, 1f, 1f, 0, 0, 1f)
        if (workoutPosition > 0) {
            binding.ivPrevious.visibility = View.VISIBLE
        } else {
            binding.ivPrevious.visibility = View.INVISIBLE
        }
        workoutTime = (data.workoutList[position]["time"].toString().toInt()) / 1000
        val time: Float = workoutTime.toFloat()
        plusLength = (100 / time)
        binding.tvTime.text = workoutTime.toString()
        binding.bottomProgressBar.progress = 0
        handler.post(runnable)

        //top Progressbar
        binding.topProgressBar.progress = topProgress
        topProgress += topProgressPlusLength.toInt()
    }

    private fun openRelaxationDialog(data: TrainingModel, position: Int) {
        soundPool.play(soundFinish, 1f, 1f, 0, 0, 1f)
        val bundle = Bundle()
        bundle.putString("relaxationDialogTitle", data.workoutList[position]["title"].toString())
        bundle.putString("relaxationDialogTime", data.workoutList[position]["time"].toString())
        bundle.putString(
            "relaxationDialogImageName",
            data.workoutList[position]["imageName"].toString()
        )
        bundle.putString("relaxationDialogSize", data.workoutList.size.toString())
        bundle.putString("relaxationDialogPosition", position.toString())
        val relaxationFragment = RelaxationDialogFragment()
        relaxationFragment.arguments = bundle
        relaxationFragment.show(supportFragmentManager, "relaxationDialog")
        relaxationFragment.listener = object : RelaxationDialogFragment.OnDismissCancelListener {
            override fun onDismiss(dialog: DialogInterface) {
                bindItemData(data, workoutPosition)
                startWorkouts(data, workoutPosition)
            }
        }
    }

    private fun openInfoDialog(hashMap: HashMap<String, Objects>) {
        val bundle = Bundle()
        bundle.putString("infoDialogTitle", hashMap["title"].toString())
        bundle.putString("infoDialogDescription", hashMap["description"].toString())
        bundle.putString("infoDialogImageName", hashMap["imageName"].toString())
        val infoFragment = InfoDialogFragment()
        infoFragment.arguments = bundle
        val transaction = supportFragmentManager.beginTransaction()
        infoFragment.listener = object : InfoDialogFragment.OnDismissListener {
            override fun onDismiss(dialogFragment: InfoDialogFragment) {
                handler.post(runnable)
            }
        }
        transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
        transaction
            .add(android.R.id.content, infoFragment)
            .addToBackStack(null)
            .commit()
    }

    private fun openExitDialog() {
        val exitFragment = ExitDialogFragment()
        exitFragment.show(supportFragmentManager, "exitDialogFragment")
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt("seconds", seconds)
        outState.putBoolean("running", running)
        outState.putBoolean("wasRunning", wasRunning)
    }

    override fun onPause() {
        super.onPause()
        wasRunning = running
        running = false
    }

    override fun onResume() {
        super.onResume()
        if (wasRunning) running = true
    }

    override fun onStop() {
        super.onStop()
        mCountDownTimer?.cancel()
        handler.removeCallbacks(runnable)
    }

    override fun onRestart() {
        super.onRestart()
        if (!startTimeFinish) {
            mCountDownTimer?.onFinish()
        } else {
            handler.post(runnable)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mCountDownTimer?.cancel()
        handler.removeCallbacks(runnable)
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(LocaleHelper.onAttach(newBase))
    }

    override fun onBackPressed() {
        openExitDialog()
//        super.onBackPressed()
    }
}