package com.artmedia.fitness.ui.main.trainingPlanItem

import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.fragment.app.DialogFragment
import com.bumptech.glide.Glide
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import com.kulbaev.fitness.R
import kotlinx.android.synthetic.main.fragment_description_dialog.*
import java.util.*

class DescriptionDialogFragment : DialogFragment() {
    private val storageRef = Firebase.storage.reference
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_description_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val title = arguments?.getString("descDialogTitle")
        val description = arguments?.getString("descDialogDescription")
        val imageName = arguments?.getString("descDialogImageName")
        tv_title.text = title
        tv_description.text = description
        downloadImage(imageName!!)

        iv_close.setOnClickListener {
            dismiss()
        }
    }

    private fun downloadImage(filename: String) {
        val pathUri = "file:///android_asset/workouts/${filename}"
        Glide.with(this)
            .load(pathUri)
            .error(R.drawable.default_image)
            .into(iv_image)
//        val islandRef = storageRef.child("images/workouts/$filename")
//        islandRef.downloadUrl.addOnSuccessListener {
//            Timber.d("success download image")
//            Glide.with(this)
//                .load(it)
//                .thumbnail(0.1f)
//                .transition(DrawableTransitionOptions.withCrossFade())
//                .diskCacheStrategy(DiskCacheStrategy.DATA)
//                .into(iv_image)
//        }
//            .addOnFailureListener { exception ->
//                Timber.w(exception, "Error.")
//            }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        return dialog
    }

    override fun onResume() {
        super.onResume()
        val width = resources.getDimensionPixelSize(R.dimen.popup_width)
        val height = resources.getDimensionPixelSize(R.dimen.popup_height)
        dialog?.window?.setLayout(width, height)
    }

    companion object {
        fun newInstance(map: HashMap<String, Objects>): DialogFragment {
            val f = DescriptionDialogFragment()
            val bundle = Bundle()
            bundle.putString("descDialogTitle", map["title"].toString())
            bundle.putString("descDialogDescription", map["description"].toString())
            bundle.putString("descDialogImageName", map["imageName"].toString())
            f.arguments = bundle
            return f
        }
    }
}