package com.artmedia.fitness.ui.main

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.navigateUp
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.android.billingclient.api.*
import com.artmedia.fitness.App
import com.artmedia.fitness.libs.ConnectionLiveData
import com.artmedia.fitness.libs.LocaleHelper
import com.artmedia.fitness.util.SecurityPurchase
import com.google.android.gms.ads.*
import com.google.android.gms.ads.interstitial.InterstitialAd
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback
import com.google.android.gms.ads.nativead.MediaView
import com.google.android.gms.ads.nativead.NativeAd
import com.google.android.gms.ads.nativead.NativeAdOptions
import com.google.android.gms.ads.nativead.NativeAdView
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.google.android.material.button.MaterialButton
import com.google.android.material.navigation.NavigationView
import com.google.android.material.snackbar.Snackbar
import com.google.android.play.core.appupdate.AppUpdateManagerFactory
import com.google.android.play.core.install.model.AppUpdateType
import com.google.android.play.core.install.model.InstallStatus
import com.google.android.play.core.install.model.UpdateAvailability
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import com.kulbaev.fitness.R
import com.kulbaev.fitness.databinding.ActivityMainBinding
import kotlinx.android.synthetic.main.app_bar_main.view.*
import kotlinx.android.synthetic.main.back_bottom_dialog.view.*
import kotlinx.android.synthetic.main.content_main.*
import timber.log.Timber
import java.io.IOException
import java.util.*

private const val UPDATE_REQUEST_CODE = 123

class MainActivity : AppCompatActivity(), PurchasesUpdatedListener {
    private lateinit var appBarConfiguration: AppBarConfiguration
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    private lateinit var binding: ActivityMainBinding
    private var dialog: BottomSheetDialog? = null
    private var doubleBackToExitPressedOnce = false
    private lateinit var adLoader: AdLoader
    var mInterstitialAd: InterstitialAd? = null
    private lateinit var connectionLiveData: ConnectionLiveData
    private val mAppUpdateManager by lazy { AppUpdateManagerFactory.create(this) }
    private lateinit var billingClient: BillingClient

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        setSupportActionBar(binding.root.toolbar)
        LocaleHelper.onAttach(applicationContext, App.pref.selectedLanguage)

        //google AdMobs
        MobileAds.initialize(this)

        if (!(App.pref.premiumMonthly || App.pref.premiumYearly)) {
            App.mInterstitialAd?.show(this@MainActivity)
        }

        //Subscribe check
        billingClient =
            BillingClient.newBuilder(this).enablePendingPurchases().setListener(this)
                .build()

        billingClient.startConnection(object : BillingClientStateListener {
            override fun onBillingSetupFinished(p0: BillingResult) {
                if (p0.responseCode == BillingClient.BillingResponseCode.OK) {
                    Timber.d("onBillingSetupFinished")
                    val queryPurchase = billingClient.queryPurchases(BillingClient.SkuType.SUBS)
                    val queryPurchases = queryPurchase.purchasesList
                    if (queryPurchases != null && queryPurchases.size > 0) {
                        Log.d("LogFirstFragment", "onBillingSetupFinished 1:true")
                        handlePurchases(queryPurchases)
                    }

                    //check which items are in purchase list and which are not in purchase list
                    //if items that are found add them to purchaseFound
                    //check status of found items and save values to preference
                    //item which are not found simply save false values to their preference
                    //indexOf return index of item in purchase list from 0-2 (because we have 3 items) else returns -1 if not found
                    val purchaseFound = ArrayList<Int>()
                    if (queryPurchases != null && queryPurchases.size > 0) {
                        //check item in purchase list
                        for (p in queryPurchases) {
                            val index = subscribeItemIDs.indexOf(p.sku)
                            //if purchase found
                            if (index > -1) {
                                purchaseFound.add(index)
                                if (p.purchaseState == Purchase.PurchaseState.PURCHASED) {
                                    saveSubscribeValue(index, true)
                                } else {
                                    saveSubscribeValue(index, false)
                                }
                            }
                        }

                        //items that are not found in purchase list mark false
                        //indexOf returns -1 when item is not in foundlist
                        for (i in subscribeItemIDs.indices) {
                            if (purchaseFound.indexOf(i) == -1) {
                                saveSubscribeValue(i, false)
                            }
                        }
                    } else {
                        for (purchaseItem in 0 until subscribeItemIDs.size) {
                            saveSubscribeValue(purchaseItem, false)
                        }
                    }
                }
            }

            override fun onBillingServiceDisconnected() {
                Toast.makeText(
                    this@MainActivity,
                    "Service Disconnected",
                    Toast.LENGTH_SHORT
                ).show()
            }
        })

        val drawerLayout: DrawerLayout = binding.drawerLayout
        val navView: NavigationView = binding.navView
        val navController = findNavController(R.id.nav_host_fragment)
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_home, R.id.nav_reminder, R.id.nav_language_selection, R.id.nav_settings
            ), drawerLayout
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        navView.setupWithNavController(navController)

        connectionLiveData = ConnectionLiveData(this)
        connectionLiveData.observe(this, { isNetworkAvailable ->
            // Update UI
            if (isNetworkAvailable) {
//                network_banner.visibility = View.GONE
            } else {
//                network_banner.visibility = View.VISIBLE
            }
        })
        //create dialog
        dialog = BottomSheetDialog(this)
        openBackDialog()

        //Google Analytics
        firebaseAnalytics = Firebase.analytics

        //App update function
        mAppUpdateManager.registerListener {
            if (it.installStatus() == InstallStatus.DOWNLOADED) {
                showCompletedUpdate()
            }
        }
        mAppUpdateManager.appUpdateInfo.addOnSuccessListener { appUpdateInfo ->
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE &&
                appUpdateInfo.isUpdateTypeAllowed(AppUpdateType.FLEXIBLE)
            ) {
                mAppUpdateManager.startUpdateFlowForResult(
                    appUpdateInfo,
                    AppUpdateType.FLEXIBLE,
                    this,
                    UPDATE_REQUEST_CODE
                )
            }
        }.addOnFailureListener {
//            Toast.makeText(this, getString(R.string.something_went_wrong), Toast.LENGTH_LONG)
//                .show()
        }
    }

    private fun handlePurchases(purchases: List<Purchase>) {
        Log.d("LogFirstFragment", "handlePurchases")
        for (purchase in purchases) {
            val index = subscribeItemIDs.indexOf(purchase.sku)
            //purchase found
            if (index > -1) {
                if (purchase.purchaseState == Purchase.PurchaseState.PURCHASED) {
                    if (!verifyValidSignature(purchase.originalJson, purchase.signature)) {
                        // Invalid purchase
                        // show error to user
                        Toast.makeText(
                            this,
                            "Error : invalid Purchase",
                            Toast.LENGTH_SHORT
                        ).show()

                        //skip current iteration only because other items in purchase list must be checked if present
                        continue
                    }
                    // else purchase is valid
                    //if item is purchased and not acknowledged
                    if (!purchase.isAcknowledged) {
                        val acknowledgePurchaseParams = AcknowledgePurchaseParams.newBuilder()
                            .setPurchaseToken(purchase.purchaseToken)
                            .build()
                        billingClient.acknowledgePurchase(acknowledgePurchaseParams) { billingResult ->
                            if (billingResult.responseCode == BillingClient.BillingResponseCode.OK) {
                                saveSubscribeValue(index, true)
                            }
                        }
                        Log.d("LogFirstFragment", "!purchase.isAcknowledged")
                    }
                    //else item is purchased and also acknowledged
                    else {
                        // Grant entitlement to the user on item purchase
                        // restart activity
                        if (!getSubscribeValue(index)) {
                            saveSubscribeValue(index, true)
                            Toast.makeText(this, "Subscribe Item Purchased", Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
                }
                //if purchase is pending
                else if (purchase.purchaseState == Purchase.PurchaseState.PENDING) {
                    Toast.makeText(
                        this,
                        "Purchase is Pending. Please complete Transaction", Toast.LENGTH_SHORT
                    ).show()
                }
                //if purchase is unknown mark false
                else if (purchase.purchaseState == Purchase.PurchaseState.UNSPECIFIED_STATE) {
                    saveSubscribeValue(index, false)
                    Toast.makeText(
                        this,
                        "Purchase Status Unknown", Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }

    private fun saveSubscribeValue(index: Int, value: Boolean) {
        if (index == 0) App.pref.premiumMonthly = value
        else App.pref.premiumYearly = value
    }

    private fun getSubscribeValue(index: Int): Boolean {
        return if (index == 0) App.pref.premiumMonthly
        else App.pref.premiumYearly
    }

    private fun showCompletedUpdate() {
        val contextView = findViewById<View>(R.id.context_view)
        Snackbar.make(contextView, R.string.new_app_is_ready, Snackbar.LENGTH_INDEFINITE)
            .setAction(R.string.install) {
                mAppUpdateManager.completeUpdate()
            }
            .show()
    }

    override fun onResume() {
        super.onResume()
        mAppUpdateManager.appUpdateInfo.addOnSuccessListener { appUpdateInfo ->
            if (appUpdateInfo.installStatus() == InstallStatus.DOWNLOADED) {
                showCompletedUpdate()
            }
        }
    }

    private fun openBackDialog() {
        val sheetView = layoutInflater.inflate(R.layout.back_bottom_dialog, null)
        loadNativeAd(sheetView)
        dialog?.setContentView(sheetView)
        sheetView.btn_back.setOnClickListener {
            finish()
        }
    }

    private fun loadNativeAd(sheetView: View) {
        adLoader = AdLoader.Builder(this, "ca-app-pub-7338795990329943/2639645909")
            .forNativeAd { nativeAd ->
                // Show the ad
                val adView =
                    layoutInflater.inflate(R.layout.native_ad_layout, null) as NativeAdView
                populateNativeAd(nativeAd, adView)
                adView.setNativeAd(nativeAd)
                sheetView.ad_container.removeAllViews()
                sheetView.ad_container.addView(adView)
                if (isDestroyed) {
                    nativeAd.destroy()
                    return@forNativeAd
                }
            }
            .withAdListener(object : AdListener() {
                override fun onAdFailedToLoad(adError: LoadAdError) {
                    // Handle the failure by logging, altering the UI, and so on.
                }
            })
            .withNativeAdOptions(
                NativeAdOptions.Builder()
                    .build()
            )
            .build()
        adLoader.loadAd(AdRequest.Builder().build())
    }

    private fun populateNativeAd(nativeAd: NativeAd, adView: NativeAdView) {
        val headlineView = adView.findViewById<TextView>(R.id.ad_headline)
        val advertiserView = adView.findViewById<TextView>(R.id.ad_description)
        val bodyView = adView.findViewById<TextView>(R.id.ad_body_text)
        val starRatingView = adView.findViewById<RatingBar>(R.id.start_rating)
        val mediaView = adView.findViewById<MediaView>(R.id.media_view)
        val callToActionView = adView.findViewById<MaterialButton>(R.id.add_call_to_action)
        val iconView = adView.findViewById<ImageView>(R.id.ad_app_icon)

        headlineView.text = nativeAd.headline
        advertiserView.text = nativeAd.advertiser
        bodyView.text = nativeAd.body
        callToActionView.text = nativeAd.callToAction
        if (nativeAd.icon != null) {
            iconView.visibility = View.VISIBLE
            iconView.setImageDrawable(nativeAd.icon!!.drawable)
        } else {
            iconView.visibility = View.GONE
        }
        if (nativeAd.mediaContent != null) {
            mediaView.visibility = View.VISIBLE
            mediaView.setMediaContent(nativeAd.mediaContent!!)
        } else {
            mediaView.visibility = View.GONE
        }
        if (nativeAd.starRating != null) {
            starRatingView.rating = nativeAd.starRating!!.toFloat()
        } else {
            starRatingView.visibility = View.INVISIBLE
        }

        adView.headlineView = headlineView
        adView.advertiserView = advertiserView
        adView.bodyView = bodyView
        adView.starRatingView = starRatingView
        adView.mediaView = mediaView
        adView.callToActionView = callToActionView
        adView.iconView = iconView
    }

    private fun loadAds() {
        Timber.d("loadAds")
        val adRequest = AdRequest.Builder().build()
        InterstitialAd.load(
            this,
            "ca-app-pub-7338795990329943/1209228325",
            adRequest,
            object : InterstitialAdLoadCallback() {
                override fun onAdFailedToLoad(adError: LoadAdError) {
                    Timber.d(adError.message)
                    mInterstitialAd = null
//                    Toast.makeText(
//                        this@MainActivity,
//                        getString(R.string.connection_off),
//                        Toast.LENGTH_SHORT
//                    ).show()
                }

                override fun onAdLoaded(interstitialAd: InterstitialAd) {
                    Timber.d("Ad was loaded.")
                    mInterstitialAd = interstitialAd
                    mInterstitialAd?.show(this@MainActivity)
                }
            })
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.action_ads -> {
                loadAds()
                true
            }
            else -> false
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        val navController = findNavController(R.id.nav_host_fragment)
        return navController.navigateUp(appBarConfiguration) || super.onSupportNavigateUp()
    }

    override fun onRestart() {
        super.onRestart()
        doubleBackToExitPressedOnce = false
    }

    override fun onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed()
            return
        }
        doubleBackToExitPressedOnce = true
        if (!(App.pref.premiumMonthly || App.pref.premiumYearly)) {
            dialog?.show()
        } else {
            Toast.makeText(this, getString(R.string.click_back_again_to_exit), Toast.LENGTH_SHORT)
                .show()
            Handler(Looper.getMainLooper()).postDelayed({
                doubleBackToExitPressedOnce = false
            }, 2000)
        }
    }

    /**
     * Verifies that the purchase was signed correctly for this developer's public key.
     * <p>Note: It's strongly recommended to perform such check on your backend since hackers can
     * replace this method with "constant true" if they decompile/rebuild your app.
     * </p>
     */
    private fun verifyValidSignature(signedData: String, signature: String): Boolean {
        return try {
            // To get key go to Developer Console > Select your app > Development Tools > Services & APIs.
            val base64Key =
                "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA6m1Onwv1+mlqz8ALd958yTCV1HlMydg+SHpS3/moycNWjByJ3gxXpidIBShJGXJBF9Arb5y+ClC4LUvRHtmybAwptxbX4dknJXSnjhazcnJ0AfzJcvpgHgUVvVLl/psoWh+w1kc5adkS4+fa1WI1z50XmrEnYckZpxe7EtvaKym8UbjAK2/CAYl5rQye5ro9tGnehtJRDT+Gs65UtGw5gQ5/aetGltGMMb1a3Nx2Ha+Fz+pULZUoljtECT7aRTNfe5ts7ffqVTOMG+8fBQE4h9LBdi4JmeH0TTgRFoqIEgwMNQc0BwGqSg+yeAptAJ7nxO+FhIEhr5UVF3ChqVZMAQIDAQAB"
            SecurityPurchase.verifyPurchase(base64Key, signedData, signature)
        } catch (e: IOException) {
            false
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        dialog = null
        billingClient.endConnection()
    }

    companion object {
        private val subscribeItemIDs: ArrayList<String> = object : ArrayList<String>() {
            init {
                add("depl_apps_stretching_exercises_monthly")
                add("depl_apps_stretching_exercises_yearly")
            }
        }
    }

    override fun onPurchasesUpdated(
        billingResult: BillingResult,
        purchases: MutableList<Purchase>?
    ) {
        //if item newly purchased
        if (billingResult.responseCode == BillingClient.BillingResponseCode.OK && purchases != null) {
            handlePurchases(purchases)
        }
        //if item already subscribed then check and reflect changes
        else {
            if (billingResult.responseCode == BillingClient.BillingResponseCode.ITEM_ALREADY_OWNED) {
                val queryAlreadyPurchasesResult: Purchase.PurchasesResult =
                    billingClient.queryPurchases(BillingClient.SkuType.SUBS)
                val alreadyPurchases: List<Purchase>? = queryAlreadyPurchasesResult.purchasesList
                if (alreadyPurchases != null) {
                    handlePurchases(alreadyPurchases)
                }
            }
            //if Purchase canceled
            else if (billingResult.responseCode == BillingClient.BillingResponseCode.USER_CANCELED) {
                Toast.makeText(this, "Purchase Canceled", Toast.LENGTH_SHORT).show()
            }
            // Handle any other error message
            else {
                Toast.makeText(
                    this,
                    "Error " + billingResult.debugMessage,
                    Toast.LENGTH_SHORT
                ).show()
                Log.d("LogFirstFragment", "Error billingResult: ${billingResult.debugMessage}")
            }
        }
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(LocaleHelper.onAttach(newBase))
    }
}