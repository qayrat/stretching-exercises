package com.artmedia.fitness.ui.main.getPremium

import android.os.Bundle
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.android.billingclient.api.*
import com.artmedia.fitness.App
import com.artmedia.fitness.util.SecurityPurchase
import com.kulbaev.fitness.R
import kotlinx.android.synthetic.main.activity_premium.*
import timber.log.Timber
import java.io.IOException

class PremiumActivity : AppCompatActivity(), PurchasesUpdatedListener {
    private lateinit var billingClient: BillingClient
    private val month = "month"
    private val year = "year"
    private var rate: String = year

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_premium)
        Timber.d("onCreate")
        billingClient =
            BillingClient.newBuilder(this).enablePendingPurchases().setListener(this)
                .build()
        billingClient.startConnection(object : BillingClientStateListener {
            override fun onBillingSetupFinished(p0: BillingResult) {
                if (p0.responseCode == BillingClient.BillingResponseCode.OK) {
                    Timber.d("onBillingSetupFinished")
                    val queryPurchase = billingClient.queryPurchases(BillingClient.SkuType.SUBS)
                    val queryPurchases = queryPurchase.purchasesList
                    if (queryPurchases != null && queryPurchases.size > 0) {
                        Log.d("LogFirstFragment", "onBillingSetupFinished 1:true")
                        handlePurchases(queryPurchases)
                    }

                    //check which items are in purchase list and which are not in purchase list
                    //if items that are found add them to purchaseFound
                    //check status of found items and save values to preference
                    //item which are not found simply save false values to their preference
                    //indexOf return index of item in purchase list from 0-2 (because we have 3 items) else returns -1 if not found
                    val purchaseFound = ArrayList<Int>()
                    if (queryPurchases != null && queryPurchases.size > 0) {
                        //check item in purchase list
                        for (p in queryPurchases) {
                            val index = subscribeItemIDs.indexOf(p.sku)
                            //if purchase found
                            if (index > -1) {
                                purchaseFound.add(index)
                                if (p.purchaseState == Purchase.PurchaseState.PURCHASED) {
                                    saveSubscribeValue(index, true)
                                } else {
                                    saveSubscribeValue(index, false)
                                }
                            }
                        }

                        //items that are not found in purchase list mark false
                        //indexOf returns -1 when item is not in foundlist
                        for (i in subscribeItemIDs.indices) {
                            if (purchaseFound.indexOf(i) == -1) {
                                saveSubscribeValue(i, false)
                            }
                        }
                    } else {
                        for (purchaseItem in 0 until subscribeItemIDs.size) {
                            saveSubscribeValue(purchaseItem, false)
                        }
                    }
                }
            }

            override fun onBillingServiceDisconnected() {
                Toast.makeText(
                    this@PremiumActivity,
                    "Service Disconnected",
                    Toast.LENGTH_SHORT
                ).show()
            }
        })

        rate = year
        subscription_month.setOnClickListener {
            rate = month
            subscription_month.setBackgroundResource(R.drawable.rate_enable_background)
            subscription_month_rb.setImageResource(R.drawable.ic_baseline_check_circle_24)
            subscription_year.setBackgroundResource(R.drawable.rate_disabled_background)
            subscription_year_rb.setImageResource(R.drawable.ic_baseline_radio_button_unchecked_24)
        }
        subscription_year.setOnClickListener {
            rate = year
            subscription_year.setBackgroundResource(R.drawable.rate_enable_background)
            subscription_year_rb.setImageResource(R.drawable.ic_baseline_check_circle_24)
            subscription_month.setBackgroundResource(R.drawable.rate_disabled_background)
            subscription_month_rb.setImageResource(R.drawable.ic_baseline_radio_button_unchecked_24)
        }
        btn_continue.setOnClickListener {
            Timber.d("rate: $rate")
            if (rate == month) {
                subscribe(subscribeItemIDs[0])
            } else {
                subscribe(subscribeItemIDs[1])
            }
        }
        iv_back.setOnClickListener {
            finish()
        }
    }

    private fun subscribe(productId: String) {
        if (billingClient.isReady) {
            initiatePurchase(productId)
            Log.d("LogFirstFragment", "billingClient.isReady")
        } else {
            billingClient = BillingClient.newBuilder(this).enablePendingPurchases()
                .setListener(this).build()
            billingClient.startConnection(object : BillingClientStateListener {
                override fun onBillingSetupFinished(p0: BillingResult) {
                    if (p0.responseCode == BillingClient.BillingResponseCode.OK) {
                        initiatePurchase(productId)
                        Log.d("LogFirstFragment", "onBillingSetupFinished: true")
                    } else {
                        Log.d("LogFirstFragment", "onBillingSetupFinished: false")
                        Toast.makeText(
                            this@PremiumActivity,
                            "Error ${p0.debugMessage}",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }

                override fun onBillingServiceDisconnected() {
                    Toast.makeText(
                        this@PremiumActivity,
                        "Service Disconnected",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            })
        }
    }

    private fun initiatePurchase(productId: String) {
        val skuList: ArrayList<String> = ArrayList()
        skuList.add(productId)
        val params: SkuDetailsParams.Builder = SkuDetailsParams.newBuilder()
        params.setSkusList(skuList).setType(BillingClient.SkuType.SUBS)
        val billingResult: BillingResult =
            billingClient.isFeatureSupported(BillingClient.FeatureType.SUBSCRIPTIONS)
        if (billingResult.responseCode == BillingClient.BillingResponseCode.OK) {
            billingClient.querySkuDetailsAsync(params.build()) { p0, p1 ->
                if (p0.responseCode == BillingClient.BillingResponseCode.OK) {
                    if (p1 != null && p1.size > 0) {
                        val flowParams = BillingFlowParams.newBuilder()
                            .setSkuDetails(p1[0])
                            .build()
                        billingClient.launchBillingFlow(this, flowParams)
                    } else {
                        Toast.makeText(
                            this,
                            "Subscribe Item not Found",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                } else {
                    Toast.makeText(
                        this,
                        "Error ${p0.debugMessage}",
                        Toast.LENGTH_SHORT
                    ).show()
                    Log.d("LogFirstFragment", "Error initiatePurchase: ${p0.debugMessage}")
                }
            }
        } else {
            Toast.makeText(
                this,
                "Sorry Subscription not Supported. Please Update Play Store",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun saveSubscribeValue(index: Int, value: Boolean) {
        if (index == 0) App.pref.premiumMonthly = value
        else App.pref.premiumYearly = value
    }

    private fun getSubscribeValue(index: Int): Boolean {
        return if (index == 0) App.pref.premiumMonthly
        else App.pref.premiumYearly
    }

    private fun handlePurchases(purchases: List<Purchase>) {
        Log.d("LogFirstFragment", "handlePurchases")
        for (purchase in purchases) {
            val index = subscribeItemIDs.indexOf(purchase.sku)
            //purchase found
            if (index > -1) {
                if (purchase.purchaseState == Purchase.PurchaseState.PURCHASED) {
                    if (!verifyValidSignature(purchase.originalJson, purchase.signature)) {
                        // Invalid purchase
                        // show error to user
                        Toast.makeText(
                            this,
                            "Error : invalid Purchase",
                            Toast.LENGTH_SHORT
                        ).show()

                        //skip current iteration only because other items in purchase list must be checked if present
                        continue
                    }
                    // else purchase is valid
                    //if item is purchased and not acknowledged
                    if (!purchase.isAcknowledged) {
                        val acknowledgePurchaseParams = AcknowledgePurchaseParams.newBuilder()
                            .setPurchaseToken(purchase.purchaseToken)
                            .build()
                        billingClient.acknowledgePurchase(acknowledgePurchaseParams) { billingResult ->
                            if (billingResult.responseCode == BillingClient.BillingResponseCode.OK) {
                                saveSubscribeValue(index, true)
                            }
                        }
                        Log.d("LogFirstFragment", "!purchase.isAcknowledged")
                    }
                    //else item is purchased and also acknowledged
                    else {
                        // Grant entitlement to the user on item purchase
                        // restart activity
                        if (!getSubscribeValue(index)) {
                            saveSubscribeValue(index, true)
                            Toast.makeText(this, "Item Purchased", Toast.LENGTH_SHORT)
                                .show()
                        }
                    }
                }
                //if purchase is pending
                else if (purchase.purchaseState == Purchase.PurchaseState.PENDING) {
                    Toast.makeText(
                        this,
                        "Purchase is Pending. Please complete Transaction", Toast.LENGTH_SHORT
                    ).show()
                }
                //if purchase is unknown mark false
                else if (purchase.purchaseState == Purchase.PurchaseState.UNSPECIFIED_STATE) {
                    saveSubscribeValue(index, false)
                    Toast.makeText(
                        this,
                        "Purchase Status Unknown", Toast.LENGTH_SHORT
                    ).show()
                }
            }
        }
    }

    /**
     * Verifies that the purchase was signed correctly for this developer's public key.
     * <p>Note: It's strongly recommended to perform such check on your backend since hackers can
     * replace this method with "constant true" if they decompile/rebuild your app.
     * </p>
     */
    private fun verifyValidSignature(signedData: String, signature: String): Boolean {
        return try {
            // To get key go to Developer Console > Select your app > Development Tools > Services & APIs.
            val base64Key =
                "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA6m1Onwv1+mlqz8ALd958yTCV1HlMydg+SHpS3/moycNWjByJ3gxXpidIBShJGXJBF9Arb5y+ClC4LUvRHtmybAwptxbX4dknJXSnjhazcnJ0AfzJcvpgHgUVvVLl/psoWh+w1kc5adkS4+fa1WI1z50XmrEnYckZpxe7EtvaKym8UbjAK2/CAYl5rQye5ro9tGnehtJRDT+Gs65UtGw5gQ5/aetGltGMMb1a3Nx2Ha+Fz+pULZUoljtECT7aRTNfe5ts7ffqVTOMG+8fBQE4h9LBdi4JmeH0TTgRFoqIEgwMNQc0BwGqSg+yeAptAJ7nxO+FhIEhr5UVF3ChqVZMAQIDAQAB"
            SecurityPurchase.verifyPurchase(base64Key, signedData, signature)
        } catch (e: IOException) {
            false
        }
    }

    companion object {
        private val subscribeItemIDs: ArrayList<String> = object : ArrayList<String>() {
            init {
                add("depl_apps_stretching_exercises_monthly")
                add("depl_apps_stretching_exercises_yearly")
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        billingClient.endConnection()
    }

    override fun onPurchasesUpdated(
        billingResult: BillingResult,
        purchases: MutableList<Purchase>?
    ) {
        //if item newly purchased
        if (billingResult.responseCode == BillingClient.BillingResponseCode.OK && purchases != null) {
            handlePurchases(purchases)
        }
        //if item already subscribed then check and reflect changes
        else {
            if (billingResult.responseCode == BillingClient.BillingResponseCode.ITEM_ALREADY_OWNED) {
                val queryAlreadyPurchasesResult: Purchase.PurchasesResult =
                    billingClient.queryPurchases(BillingClient.SkuType.SUBS)
                val alreadyPurchases: List<Purchase>? = queryAlreadyPurchasesResult.purchasesList
                if (alreadyPurchases != null) {
                    handlePurchases(alreadyPurchases)
                }
            }
            //if Purchase canceled
            else if (billingResult.responseCode == BillingClient.BillingResponseCode.USER_CANCELED) {
                Toast.makeText(this, "Purchase Canceled", Toast.LENGTH_SHORT).show()
            }
            // Handle any other error message
            else {
                Toast.makeText(
                    this,
                    "Error " + billingResult.debugMessage,
                    Toast.LENGTH_SHORT
                ).show()
                Log.d("LogFirstFragment", "Error billingResult: ${billingResult.debugMessage}")
            }
        }
    }
}