package com.artmedia.fitness.ui.main.reminder

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.artmedia.fitness.App
import com.artmedia.fitness.data.model.ReminderModel
import com.artmedia.fitness.receiver.AlarmReceiver
import com.artmedia.fitness.service.AlarmService
import com.artmedia.fitness.util.Constants
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.timepicker.MaterialTimePicker
import com.google.android.material.timepicker.TimeFormat
import com.kulbaev.fitness.R
import kotlinx.android.synthetic.main.fragment_reminder.*
import timber.log.Timber
import java.util.*
import kotlin.collections.ArrayList

class ReminderFragment : Fragment(R.layout.fragment_reminder), ReminderAdapter.OnItemClickListener {

    private lateinit var viewModel: ReminderViewModel
    private lateinit var alarmService: AlarmService
    private var time = ""
    private var hour = 0
    private var minute = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        alarmService = AlarmService(App.context!!)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(ReminderViewModel::class.java)
        viewModel.getReminder()
        viewModel.list.observe(viewLifecycleOwner, Observer {
            setAdapter(it)
        })
//        fab_add.setOnClickListener {
//            openTimePicker()
//        }
    }


    private fun setAlarm(hourOfDay: Int, minute: Int, requestCode: Int) {
        Timber.d("$hourOfDay : $minute")
        val calendar = Calendar.getInstance().apply {
            timeInMillis = System.currentTimeMillis()
            set(Calendar.HOUR_OF_DAY, hourOfDay)
            set(Calendar.MINUTE, minute)
            set(Calendar.SECOND, 0)
        }
        val alarmIntent = Intent(requireContext(), AlarmReceiver::class.java).let {
            PendingIntent.getBroadcast(requireContext(), requestCode, it, 0)
        }

//        if (calendar.timeInMillis < System.currentTimeMillis()) {
//            calendar.add(Calendar.DAY_OF_YEAR, 7)
//        }
        val alarmManager = requireContext().getSystemService(Context.ALARM_SERVICE) as AlarmManager
        alarmManager.setInexactRepeating(
            AlarmManager.RTC_WAKEUP,
            calendar.timeInMillis,
            AlarmManager.INTERVAL_DAY,
            alarmIntent
        )
    }

    private fun cancelAlarm(reminder: ReminderModel, requestCode: Int) {
        val intent = Intent(context, AlarmReceiver::class.java)
        val pendingIntent = PendingIntent.getBroadcast(
            context,
            requestCode,
            intent,
            0
        )
        Timber.d("$pendingIntent")
        val alarmManager = requireContext().getSystemService(Context.ALARM_SERVICE) as AlarmManager
        alarmManager.cancel(pendingIntent)
        pendingIntent.cancel()
    }

    private fun setAdapter(data: ArrayList<ReminderModel>) {
        val reminder = ArrayList<ReminderModel>()
        reminder.add(data[0])
        val adapter = ReminderAdapter(this, reminder)
        rv_list.layoutManager =
            StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL)
        rv_list.adapter = adapter
    }

    private fun openTimePicker(reminder: ReminderModel) {
        val timePicker = MaterialTimePicker.Builder()
            .setTimeFormat(TimeFormat.CLOCK_24H)
            .setTitleText(R.string.select_time)
            .build()
        timePicker.addOnPositiveButtonClickListener {
            time = String.format("%02d:%02d", timePicker.hour, timePicker.minute)
            hour = timePicker.hour
            minute = timePicker.minute
            viewModel.updateReminderTime(reminder.id, timePicker.hour, timePicker.minute)
            viewModel.updateReminderAlarm(reminder.id, 1)
            viewModel.getReminder()
            val checkedItemsReminder = intArrayOf(
                reminder.monday,
                reminder.tuesday,
                reminder.wednesday,
                reminder.thursday,
                reminder.friday,
                reminder.saturday,
                reminder.sunday
            )
            val reminderModel = ReminderModel(
                reminder.id,
                timePicker.hour,
                reminder.alarm,
                reminder.monday,
                reminder.tuesday,
                reminder.wednesday,
                reminder.thursday,
                reminder.friday,
                reminder.saturday,
                reminder.sunday,
                timePicker.minute
            )
            updateAlarmWeek(reminderModel, checkedItemsReminder)
        }
        timePicker.addOnNegativeButtonClickListener {
            timePicker.dismiss()
        }
        timePicker.show(childFragmentManager, "timePicker")
    }

    private fun openRepeatDialog(
        reminder: ReminderModel,
        checkedItems: BooleanArray,
        checkedItemsReminder: IntArray
    ) {
        val multiItems = arrayOf(
            getString(R.string.monday), getString(R.string.tuesday), getString(R.string.wednesday),
            getString(R.string.thursday), getString(R.string.friday),
            getString(R.string.saturday), getString(R.string.sunday)
        )
        MaterialAlertDialogBuilder(requireContext())
            .setTitle(R.string.repeat)
            .setNeutralButton(getString(R.string.cancel)) { dialog, which ->
                dialog.dismiss()
            }
            .setPositiveButton(getString(R.string.ok)) { dialog, which ->
                viewModel.updateReminderWeek(reminder.id, checkedItemsReminder)
                viewModel.updateReminderAlarm(reminder.id, 1)
                viewModel.getReminder()
                updateAlarmWeek(reminder, checkedItemsReminder)
            }
            .setMultiChoiceItems(multiItems, checkedItems) { dialog, which, checked ->
                if (checked) checkedItemsReminder[which] = 1
                else checkedItemsReminder[which] = 0
            }
            .show()
    }

    private fun updateAlarmWeek(reminder: ReminderModel, checkedItemsReminder: IntArray) {
        val requestCode = intArrayOf(
            App.pref.monday,
            App.pref.tuesday,
            App.pref.wednesday,
            App.pref.thursday,
            App.pref.friday,
            App.pref.saturday,
            App.pref.sunday
        )
        val weekDays = intArrayOf(
            Calendar.MONDAY,
            Calendar.TUESDAY,
            Calendar.WEDNESDAY,
            Calendar.THURSDAY,
            Calendar.FRIDAY,
            Calendar.SATURDAY,
            Calendar.SUNDAY
        )
        setAlarm(reminder.hour, reminder.minute, 1)
//        for (i in checkedItemsReminder.indices) {
//            if (checkedItemsReminder[i] == 1) {
////                setAlarm(reminder.hour, reminder.minute, requestCode[i])
//            } else {
////                cancelAlarm(reminder, requestCode[i])
//            }
//        }
    }

    private fun openTimePicker() {
        val timePicker = MaterialTimePicker.Builder()
            .setTimeFormat(TimeFormat.CLOCK_24H)
            .setTitleText(R.string.select_time)
            .build()
        timePicker.addOnPositiveButtonClickListener {
            time = String.format("%02d:%02d", timePicker.hour, timePicker.minute)
            hour = timePicker.hour
            minute = timePicker.minute
            openRepeatDialog()
        }
        timePicker.addOnNegativeButtonClickListener {
            timePicker.dismiss()
        }
        timePicker.show(childFragmentManager, "timePicker")
    }

    private fun openRepeatDialog() {
        val multiItems = arrayOf(
            getString(R.string.monday), getString(R.string.tuesday), getString(R.string.wednesday),
            getString(R.string.thursday), getString(R.string.friday),
            getString(R.string.saturday), getString(R.string.sunday)
        )
        val checkedItemsReminder = intArrayOf(1, 1, 1, 1, 1, 1, 1)
        val checkedItems = booleanArrayOf(true, true, true, true, true, true, true)
        MaterialAlertDialogBuilder(requireContext())
            .setTitle(R.string.repeat)
            .setNeutralButton(getString(R.string.cancel)) { dialog, which ->
                dialog.dismiss()
            }
            .setPositiveButton(getString(R.string.ok)) { dialog, which ->
                viewModel.addReminder(
                    ReminderModel(
                        1,
                        hour,
                        1,
                        checkedItemsReminder[0],
                        checkedItemsReminder[1],
                        checkedItemsReminder[2],
                        checkedItemsReminder[3],
                        checkedItemsReminder[4],
                        checkedItemsReminder[5],
                        checkedItemsReminder[6],
                        minute
                    )
                )
            }
            .setMultiChoiceItems(multiItems, checkedItems) { dialog, which, checked ->
                if (checked) checkedItemsReminder[which] = 1
                else checkedItemsReminder[which] = 0
            }
            .show()
    }

    override fun onTimeClick(reminder: ReminderModel) {
        openTimePicker(reminder)
    }

    override fun onAlarmClick(reminder: ReminderModel, isCheck: Boolean) {
        val checkedItemsReminder = intArrayOf(
            reminder.monday,
            reminder.tuesday,
            reminder.wednesday,
            reminder.thursday,
            reminder.friday,
            reminder.saturday,
            reminder.sunday
        )
        if (isCheck) {
            Timber.d("isCheck: $isCheck")
            viewModel.updateReminderAlarm(reminder.id, 1)
            updateAlarmWeek(reminder, checkedItemsReminder)
        } else {
            viewModel.updateReminderAlarm(reminder.id, 0)
            Timber.d("isCheck: $isCheck")
            val requestCode = intArrayOf(
                App.pref.monday,
                App.pref.tuesday,
                App.pref.wednesday,
                App.pref.thursday,
                App.pref.friday,
                App.pref.saturday,
                App.pref.sunday
            )
            cancelAlarm(reminder, 1)
//            for (i in requestCode.indices) {
//                Timber.d("requestCode: ${requestCode[i]}")
//                cancelAlarm(reminder, requestCode[i])
//            }
        }
    }

    override fun onWeekClick(reminder: ReminderModel) {
        val checkedItemsReminder = intArrayOf(
            reminder.monday,
            reminder.tuesday,
            reminder.wednesday,
            reminder.thursday,
            reminder.friday,
            reminder.saturday,
            reminder.sunday
        )
        val checkedItems = booleanArrayOf(
            reminder.monday == 1,
            reminder.tuesday == 1,
            reminder.wednesday == 1,
            reminder.thursday == 1,
            reminder.friday == 1,
            reminder.saturday == 1,
            reminder.sunday == 1
        )
        openRepeatDialog(reminder, checkedItems, checkedItemsReminder)
    }

    override fun onDeleteClick(reminder: ReminderModel) {
        viewModel.deleteReminder(reminder.id)
        val requestCode = intArrayOf(
            App.pref.monday,
            App.pref.tuesday,
            App.pref.wednesday,
            App.pref.thursday,
            App.pref.friday,
            App.pref.saturday,
            App.pref.sunday
        )
        for (i in requestCode.indices) {
            cancelAlarm(reminder, requestCode[i])
        }
    }
}