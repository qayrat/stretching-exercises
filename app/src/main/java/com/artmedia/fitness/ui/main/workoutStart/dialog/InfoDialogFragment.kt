package com.artmedia.fitness.ui.main.workoutStart.dialog

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.fragment.app.DialogFragment
import com.artmedia.fitness.App
import com.artmedia.fitness.libs.AdMobAdaptiveBanner
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import com.kulbaev.fitness.R
import kotlinx.android.synthetic.main.fragment_info_dialog.*
import timber.log.Timber

class InfoDialogFragment : DialogFragment() {
    private lateinit var adMob: AdMobAdaptiveBanner
    private val adUnitId = "ca-app-pub-7338795990329943/5678550824"
    private val storageRef = Firebase.storage.reference
    private var initialLayoutComplete = false
    var listener: OnDismissListener? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_info_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adMob =
            AdMobAdaptiveBanner.newInstance(
                requireContext(),
                requireActivity(),
                ad_view_container,
                adUnitId
            )
        if (!(App.pref.premiumMonthly || App.pref.premiumYearly)) {
            ad_view_container.addView(adMob.getAdView())
            ad_view_container.viewTreeObserver.addOnGlobalLayoutListener {
                if (!initialLayoutComplete) {
                    initialLayoutComplete = true
                    adMob.getBanner()
                }
            }
        }
        val title = arguments?.getString("infoDialogTitle")
        val description = arguments?.getString("infoDialogDescription")
        val imageName = arguments?.getString("infoDialogImageName")
        tv_title.text = title
        tv_description.text = description
        Timber.d(imageName)
        downloadImage(imageName!!)
        iv_back.setOnClickListener {
            dismiss()
        }
    }

    private fun downloadImage(filename: String) {
        val pathUri = "file:///android_asset/workouts/${filename}"
        Glide.with(this)
            .load(pathUri)
            .error(R.drawable.default_image)
            .into(image_view)
//        val islandRef = storageRef.child("images/workouts/$filename")
//        islandRef.downloadUrl.addOnSuccessListener {
//            Timber.d("success download image")
//            Glide.with(this)
//                .load(it)
//                .thumbnail(0.1f)
//                .transition(DrawableTransitionOptions.withCrossFade())
//                .diskCacheStrategy(DiskCacheStrategy.DATA)
//                .into(image_view)
//        }
//            .addOnFailureListener { exception ->
//                Timber.w(exception, "Error.")
//            }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        return dialog
    }

    /** Called when leaving the activity  */
    override fun onPause() {
        adMob.getAdView().pause()
        super.onPause()
    }

    /** Called when returning to the activity  */
    override fun onResume() {
        super.onResume()
        adMob.getAdView().resume()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnDismissListener) {
            listener = context
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        listener?.onDismiss(this)
    }

    interface OnDismissListener {
        fun onDismiss(dialogFragment: InfoDialogFragment)
    }
}