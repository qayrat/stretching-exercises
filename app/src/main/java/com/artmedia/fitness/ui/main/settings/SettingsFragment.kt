package com.artmedia.fitness.ui.main.settings

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.artmedia.fitness.App
import com.artmedia.fitness.libs.AdMobAdaptiveBanner
import com.artmedia.fitness.ui.main.getPremium.PremiumActivity
import com.kulbaev.fitness.R
import kotlinx.android.synthetic.main.fragment_settings.*
import kotlinx.android.synthetic.main.fragment_settings.view.*

class SettingsFragment : Fragment() {
    private lateinit var adMob: AdMobAdaptiveBanner
    private var initialLayoutComplete = false
    private val adUnitId = "ca-app-pub-7338795990329943/5678550824"
    private val navController by lazy(LazyThreadSafetyMode.NONE) {
        Navigation.findNavController(requireActivity(), R.id.nav_host_fragment)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = layoutInflater.inflate(R.layout.fragment_settings, container, false)
        adMob =
            AdMobAdaptiveBanner.newInstance(
                requireContext(),
                requireActivity(),
                root.ad_view_container,
                adUnitId
            )
        if (!(App.pref.premiumMonthly || App.pref.premiumYearly)) {
            root.ad_view_container.addView(adMob.getAdView())
            root.ad_view_container.viewTreeObserver.addOnGlobalLayoutListener {
                if (!initialLayoutComplete) {
                    initialLayoutComplete = true
                    adMob.getBanner()
                }
            }
        }
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        when (App.pref.selectedLanguage) {
            "en" -> {
                tv_default_language.text = getString(R.string.english)
            }
            "ru" -> {
                tv_default_language.text = getString(R.string.russian)
            }
        }

        btn_premium.setOnClickListener {
            val intent = Intent(requireContext(), PremiumActivity::class.java)
            startActivity(intent)
        }

        reminder.setOnClickListener {
            navController.navigate(R.id.nav_reminder)
        }
        select_language.setOnClickListener {
            navController.navigate(R.id.nav_language_selection)
        }
        share_with_friends.setOnClickListener {
            val share = Intent.createChooser(Intent().apply {
                action = Intent.ACTION_SEND
                putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=com.artmedia.fitness")
                type = "text/*"
            }, null)
            startActivity(share)
        }
        review.setOnClickListener {
            val intent = Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.artmedia.fitness"))
            startActivity(intent)
        }
        privacy_policy.setOnClickListener {
            val intent =
                Intent(Intent.ACTION_VIEW, Uri.parse("https://telegra.ph/Privacy-Policy-08-06-17"))
            startActivity(intent)
        }
    }

    /** Called when leaving the activity  */
    override fun onPause() {
        adMob.getAdView().pause()
        super.onPause()
    }

    /** Called when returning to the activity  */
    override fun onResume() {
        super.onResume()
        adMob.getAdView().resume()
    }
}