package com.artmedia.fitness.ui.main.trainingPlanItem

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.artmedia.fitness.App
import com.artmedia.fitness.data.model.TrainingModel
import com.google.firebase.firestore.Source
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import timber.log.Timber
import java.util.*

class TrainingPlanItemViewModel : ViewModel() {
    private val _list = MutableLiveData<TrainingModel>()
    val list: LiveData<TrainingModel> = _list

    fun getTrainingItem(documentName: String) {
        //create FireStore
        val db = Firebase.firestore
        val source = Source.CACHE
        var collection = ""
        when(App.pref.selectedLanguage) {
            "en" -> { collection = "en" }
            "ru" -> {collection = "data"}
        }
        db.collection(collection).document(documentName)
            .get()
            .addOnSuccessListener { document ->
                val workoutList: List<HashMap<String, Objects>> =
                    document["workoutList"] as List<HashMap<String, Objects>>
                val item = TrainingModel(
                    document.id,
                    document["allTime"].toString().toInt(),
                    document["categoryName"].toString(),
                    document["firstItem"].toString().toBoolean(),
                    document["imageName"].toString(),
                    document["premium"].toString().toBoolean(),
                    document["showSubTitle"].toString().toBoolean(),
                    document["subTitle"].toString(),
                    document["title"].toString(),
                    workoutList,
                    document["workouts"].toString().toInt(),
                )
                _list.value = item
                Timber.d("${document.id} => ${document.data.toString()}")
            }
            .addOnFailureListener { exception ->
                Timber.w(exception, "Error getting documents.")
            }
    }
}