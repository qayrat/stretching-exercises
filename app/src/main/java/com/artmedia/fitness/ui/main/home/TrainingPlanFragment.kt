package com.artmedia.fitness.ui.main.home

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.artmedia.fitness.App
import com.artmedia.fitness.data.model.TrainingModel
import com.artmedia.fitness.libs.AdMobAdaptiveBanner
import com.artmedia.fitness.ui.main.home.adapter.TrainingPlanAdapter
import com.artmedia.fitness.ui.main.trainingPlanItem.TrainingPlanItemActivity
import com.artmedia.fitness.ui.main.trainingPlanItemLevels.TrainingPlanItemLevelsActivity
import com.bumptech.glide.Glide
import com.kulbaev.fitness.R
import com.kulbaev.fitness.databinding.FragmentHomeBinding
import timber.log.Timber
import java.util.*

class TrainingPlanFragment : Fragment(),
    TrainingPlanAdapter.OnItemClickListener {

    private lateinit var viewModel: TrainingPlanViewModel
    private var initialLayoutComplete = false
    private lateinit var adMob: AdMobAdaptiveBanner
    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!
    private val adUnitId = "ca-app-pub-7338795990329943/2672722922"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        adMob =
            AdMobAdaptiveBanner.newInstance(
                requireContext(),
                requireActivity(),
                binding.adViewContainer,
                adUnitId
            )
        if (!(App.pref.premiumMonthly || App.pref.premiumYearly)) {
            binding.adViewContainer.addView(adMob.getAdView())
            binding.adViewContainer.viewTreeObserver.addOnGlobalLayoutListener {
                if (!initialLayoutComplete) {
                    initialLayoutComplete = true
                    Timber.d("initialLayoutComplete = false")
                    adMob.getBanner()
                }
            }
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(this).get(TrainingPlanViewModel::class.java)
        viewModel.getTrainingPlans()
        viewModel.list.observe(viewLifecycleOwner, Observer {
            setAdapter(it)
        })

        binding.comeBackBeginning.setOnClickListener {
            binding.nestedScrollView.smoothScrollTo(0, 0)
        }

        binding.clickSplits.setOnClickListener {
            val intent = Intent(requireActivity(), TrainingPlanItemLevelsActivity::class.java)
            startActivity(intent)
        }
    }

    private fun setAdapter(it: ArrayList<TrainingModel>) {
        val adapter = TrainingPlanAdapter(this, it)
        binding.rvList.layoutManager =
            StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL)
        binding.rvList.adapter = adapter
        binding.comeBackBeginning.visibility = View.VISIBLE
        binding.splitsTraining.visibility = View.VISIBLE
        val pathUri = "file:///android_asset/homeImages/home16.jpg"
        Glide.with(App.context!!)
            .load(pathUri)
            .error(R.drawable.default_image)
            .into(binding.ivImageSplits)
    }

    override fun onClick(d: TrainingModel) {
        App.pref.trainingItemId = d.id
        val intent = Intent(requireActivity(), TrainingPlanItemActivity::class.java)
        intent.putExtra("trainingPlanTitle", d.title)
        intent.putExtra("trainingPlanImageName", d.imageName)
        intent.putExtra("trainingPlanPremium", d.premium)
        startActivity(intent)
    }

    /** Called when leaving the activity  */
    override fun onPause() {
        adMob.getAdView().pause()
        super.onPause()
    }

    /** Called when returning to the activity  */
    override fun onResume() {
        super.onResume()
        adMob.getAdView().resume()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}