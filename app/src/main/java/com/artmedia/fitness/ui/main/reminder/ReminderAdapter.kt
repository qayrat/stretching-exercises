package com.artmedia.fitness.ui.main.reminder

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.artmedia.fitness.data.model.ReminderModel
import com.kulbaev.fitness.R
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.fragment_reminder_item.*
import timber.log.Timber

class ReminderAdapter(
    private val fragment: Fragment,
    private val data: ArrayList<ReminderModel>
) : RecyclerView.Adapter<ReminderAdapter.ViewHolder>() {
    private val layoutInflater = LayoutInflater.from(fragment.requireContext())
    private val listener = fragment as OnItemClickListener
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = layoutInflater.inflate(R.layout.fragment_reminder_item, parent, false)
        return ViewHolder(view, listener, fragment)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bindData(data[position])

    override fun getItemCount(): Int = data.size

    class ViewHolder(
        itemView: View,
        listener: OnItemClickListener,
        private val f: Fragment
    ) : RecyclerView.ViewHolder(itemView), LayoutContainer {
        override val containerView: View
            get() = itemView
        private lateinit var itemData: ReminderModel

        init {
            tv_time.setOnClickListener {
                listener.onTimeClick(itemData)
            }
            tv_repeat_text.setOnClickListener {
                listener.onWeekClick(itemData)
            }
            tv_week_days.setOnClickListener {
                listener.onWeekClick(itemData)
            }
            switch_time.setOnClickListener {
                Timber.d("switchTime : ${switch_time.isChecked}")
                listener.onAlarmClick(itemData, switch_time.isChecked)
            }
            delete.setOnClickListener {
                listener.onDeleteClick(itemData)
            }
        }

        fun bindData(reminder: ReminderModel) {
            itemData = reminder
            val time = String.format("%02d:%02d", reminder.hour, reminder.minute)
            tv_time.text = time
            switch_time.isChecked = reminder.alarm == 1
            tv_week_days.text = getWeekDays(reminder)
        }

        private fun getWeekDays(reminder: ReminderModel): String {
            var weekDays = ""
            val reminderList = arrayOf(
                reminder.monday, reminder.tuesday, reminder.wednesday, reminder.thursday,
                reminder.friday, reminder.saturday, reminder.sunday
            )
            val list = arrayOf(
                f.getString(R.string.monday_short), f.getString(R.string.tuesday_short),
                f.getString(R.string.wednesday_short), f.getString(R.string.thursday_short),
                f.getString(R.string.friday_short), f.getString(R.string.saturday_short),
                f.getString(R.string.sunday_short)
            )
            for (i in reminderList.indices) {
                if (reminderList[i] == 1) {
                    weekDays += list[i] + ", "
                }
            }
            weekDays = weekDays.removeRange(weekDays.length - 2, weekDays.length - 1)
            return weekDays
        }
    }

    interface OnItemClickListener {
        fun onTimeClick(reminder: ReminderModel)
        fun onAlarmClick(reminder: ReminderModel, isCheck: Boolean)
        fun onWeekClick(reminder: ReminderModel)
        fun onDeleteClick(reminder: ReminderModel)
    }
}