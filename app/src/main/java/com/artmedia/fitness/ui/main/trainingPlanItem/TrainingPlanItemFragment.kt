package com.artmedia.fitness.ui.main.trainingPlanItem

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.artmedia.fitness.App
import com.artmedia.fitness.data.model.TrainingModel
import com.artmedia.fitness.libs.AdMobAdaptiveBanner
import com.artmedia.fitness.ui.main.trainingPlanItem.adapter.TrainingPlanItemAdapter
import com.artmedia.fitness.ui.main.workoutStart.WorkoutActivity
import com.kulbaev.fitness.R
import com.kulbaev.fitness.databinding.FragmentTrainingPlanItemBinding
import kotlinx.android.synthetic.main.activity_training_plan_item.*
import timber.log.Timber
import java.util.*

class TrainingPlanItemFragment : Fragment(R.layout.fragment_training_plan_item),
    TrainingPlanItemAdapter.OnItemClickListener {
    private lateinit var viewModel: TrainingPlanItemViewModel
    private lateinit var adMob: AdMobAdaptiveBanner
    private var initialLayoutComplete = false
    private var _binding: FragmentTrainingPlanItemBinding? = null
    private val binding get() = _binding!!
    private val adUnitId = "ca-app-pub-7338795990329943/5138467104"

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentTrainingPlanItemBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Timber.d("onViewCreated")
        //create adView
        adMob = AdMobAdaptiveBanner.newInstance(
            requireContext(),
            requireActivity(),
            requireActivity().ad_view_container,
            adUnitId
        )
        if (!(App.pref.premiumMonthly || App.pref.premiumYearly)) {
            requireActivity().ad_view_container.addView(adMob.getAdView())
            requireActivity().ad_view_container.viewTreeObserver.addOnGlobalLayoutListener {
                if (!initialLayoutComplete) {
                    initialLayoutComplete = true
                    adMob.getBanner()
                }
            }
        }
        viewModel = ViewModelProvider(this).get(TrainingPlanItemViewModel::class.java)
        viewModel.getTrainingItem(App.pref.trainingItemId)
        viewModel.list.observe(viewLifecycleOwner, Observer {
            App.pref.trainingItemId = it.id
            setData(it)
        })

        requireActivity().tv_start.setOnClickListener {
            val intent = Intent(requireActivity(), WorkoutActivity::class.java)
            startActivity(intent)
        }
        requireActivity().start_btn_background.setOnClickListener { }
    }

    private fun showDialog(map: HashMap<String, Objects>) {
        val descFragment = DescriptionDialogFragment.newInstance(map)
        descFragment.show(childFragmentManager, "descDialog")
    }

    private fun setData(it: TrainingModel) {
        requireActivity().tv_title.text = it.title
        requireActivity().tv_sub_title.text = it.subTitle
        binding.tvTime.text = it.allTime.toString()
        binding.tvWorkout.text = it.workouts.toString()
        val adapter = TrainingPlanItemAdapter(this, it.workoutList)
        binding.rvList.layoutManager =
            StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.VERTICAL)
        binding.rvList.adapter = adapter
    }

    override fun onClick(map: HashMap<String, Objects>) {
        showDialog(map)
    }

    /** Called when leaving the activity  */
    override fun onPause() {
        adMob.getAdView().pause()
        super.onPause()
    }

    /** Called when returning to the activity  */
    override fun onResume() {
        super.onResume()
        adMob.getAdView().resume()
    }

    companion object {
        fun newInstance() = TrainingPlanItemFragment()
    }
}