package com.artmedia.fitness.ui.main.reminder

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.artmedia.fitness.data.database.DBHelper
import com.artmedia.fitness.data.model.ReminderModel

class ReminderViewModel : ViewModel() {
    private val _list = MutableLiveData<ArrayList<ReminderModel>>()
    val list: LiveData<ArrayList<ReminderModel>> = _list

    fun getReminder() {
        _list.value = DBHelper.getDatabase().reminder
    }

    fun addReminder(d: ReminderModel) {
        DBHelper.getDatabase().insertReminder(d)
        getReminder()
    }

    fun deleteReminder(id: Int) {
        DBHelper.getDatabase().deleteReminder(id)
        getReminder()
    }

    fun updateReminderTime(id: Int, hour: Int, minute: Int) {
        DBHelper.getDatabase().updateReminderTime(id, hour, minute)
        getReminder()
    }

    fun updateReminderAlarm(id: Int, alarm: Int) {
        DBHelper.getDatabase().updateReminderAlarm(id, alarm)
        getReminder()
    }

    fun updateReminderWeek(id: Int, week: IntArray) {
        DBHelper.getDatabase().updateReminderWeek(id, week)
        getReminder()
    }
}