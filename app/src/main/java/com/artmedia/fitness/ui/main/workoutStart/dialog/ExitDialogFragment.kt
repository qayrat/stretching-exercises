package com.artmedia.fitness.ui.main.workoutStart.dialog

import android.app.Dialog
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.fragment.app.DialogFragment
import com.kulbaev.fitness.R
import kotlinx.android.synthetic.main.fragment_exit_dialog.*

class ExitDialogFragment : DialogFragment(R.layout.fragment_exit_dialog) {

    override fun onStart() {
        super.onStart()
        val d = dialog
        if (d != null) {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = resources.getDimensionPixelSize(R.dimen.dialog_height)
            d.window!!.setLayout(width, height)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btn_exit.setOnClickListener {
            dismiss()
            requireActivity().finish()
        }
        btn_proceed.setOnClickListener {
            dismiss()
        }
        iv_close.setOnClickListener {
            dismiss()
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        return dialog
    }
}