package com.artmedia.fitness.ui.main.workoutStart.dialog

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.os.CountDownTimer
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.fragment.app.DialogFragment
import com.artmedia.fitness.App
import com.artmedia.fitness.libs.AdMobAdaptiveBanner
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import com.kulbaev.fitness.R
import kotlinx.android.synthetic.main.fragment_relaxation_dialog.*
import timber.log.Timber
import java.text.SimpleDateFormat
import java.util.*

class RelaxationDialogFragment : DialogFragment() {
    private lateinit var adMob: AdMobAdaptiveBanner
    private val storageRef = Firebase.storage.reference
    private var initialLayoutComplete = false
    private var mCountDownTimer: CountDownTimer? = null
    private var allTime: Long = 15000
    var listener: OnDismissCancelListener? = null
    private val adUnitId = "ca-app-pub-7338795990329943/9107015168"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.MY_DIALOG)
    }

    override fun onStart() {
        super.onStart()
        val d = dialog
        if (d != null) {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT
            d.window!!.setLayout(width, height)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_relaxation_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val title = arguments?.getString("relaxationDialogTitle")
        val time = arguments?.getString("relaxationDialogTime")
        val imageName = arguments?.getString("relaxationDialogImageName")
        val size = arguments?.getString("relaxationDialogSize")
        val position = arguments?.getString("relaxationDialogPosition")
        tv_title.text = title
        tv_time.text = convertTime(time!!.toLong())
        val workoutPosition = position!!.toInt() + 1
        tv_next_text.text = "${getString(R.string.following)} $workoutPosition/$size"
        downloadImage(imageName!!)
        startTime(allTime)

        //create google adMob
        adMob =
            AdMobAdaptiveBanner.newInstance(
                requireContext(),
                requireActivity(),
                ad_view_container,
                adUnitId
            )
        if (!(App.pref.premiumMonthly || App.pref.premiumYearly)) {
            ad_view_container.addView(adMob.getAdView())
            ad_view_container.viewTreeObserver.addOnGlobalLayoutListener {
                if (!initialLayoutComplete) {
                    initialLayoutComplete = true
                    adMob.getBanner()
                }
            }
        }

        skip.setOnClickListener {
            dismiss()
        }
        add_time.setOnClickListener {
            if (allTime < 120000) {
                allTime += 20000
                mCountDownTimer?.cancel()
                startTime(allTime)
            }
        }
    }

    private fun startTime(time: Long) {
        mCountDownTimer = object : CountDownTimer(time, 1000) {
            override fun onTick(p0: Long) {
                allTime -= 1000
                Timber.d("onTick: $allTime")
                tv_timer.text = convertTime(p0)
            }

            override fun onFinish() {
                Timber.d("onFinish: $allTime")
                dismiss()
            }
        }
        mCountDownTimer?.start()
    }

    private fun downloadImage(filename: String) {
        val pathUri = "file:///android_asset/workouts/${filename}"
        Glide.with(this)
            .load(pathUri)
            .error(R.drawable.default_image)
            .into(iv_image)
//        val islandRef = storageRef.child("images/workouts/$filename")
//        islandRef.downloadUrl.addOnSuccessListener {
//            Timber.d("success download image")
//            Glide.with(this)
//                .load(it)
//                .thumbnail(0.1f)
//                .transition(DrawableTransitionOptions.withCrossFade())
//                .diskCacheStrategy(DiskCacheStrategy.DATA)
//                .into(iv_image)
//        }
//            .addOnFailureListener { exception ->
//                Timber.w(exception, "Error.")
//            }
    }

    @SuppressLint("SimpleDateFormat")
    private fun convertTime(time: Long): String {
        return SimpleDateFormat("mm:ss").format(Date(time))
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        return dialog
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        mCountDownTimer?.cancel()
        Timber.d("onDismiss")
        listener?.onDismiss(dialog)
    }

    /** Called when leaving the activity  */
    override fun onPause() {
        adMob.getAdView().pause()
        super.onPause()
    }

    /** Called when returning to the activity  */
    override fun onResume() {
        super.onResume()
        adMob.getAdView().resume()
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnDismissCancelListener) {
            listener = context
        }
    }

    interface OnDismissCancelListener {
        fun onDismiss(dialog: DialogInterface)
    }
}

