package com.artmedia.fitness.ui.main.trainingPlanItem.adapter

import android.annotation.SuppressLint
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.artmedia.fitness.App
import com.artmedia.fitness.libs.CacheImageManager
import com.bumptech.glide.Glide
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import com.kulbaev.fitness.R
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.training_plan_item.*
import timber.log.Timber
import java.io.File
import java.text.SimpleDateFormat
import java.util.*

class TrainingPlanItemAdapter(
    private val fragment: Fragment,
    private val data: List<HashMap<String, Objects>>
) : RecyclerView.Adapter<TrainingPlanItemAdapter.ViewHolder>() {
    private val layoutInflater = LayoutInflater.from(fragment.requireContext())
    private val listener = fragment as OnItemClickListener

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val view = layoutInflater.inflate(R.layout.training_plan_item, parent, false)
        return ViewHolder(view, listener, fragment)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bindData(data[position])

    override fun getItemCount(): Int = data.size

    inner class ViewHolder(
        itemView: View,
        listener: OnItemClickListener,
        private var fragment: Fragment
    ) : RecyclerView.ViewHolder(itemView), LayoutContainer {
        override val containerView: View
            get() = itemView
        private lateinit var itemData: HashMap<String, Objects>
        private val storageRef = Firebase.storage.reference
        private final var TAG = "TrainingPlanItemViewHolder"

        init {
            item_click.setOnClickListener {
                listener.onClick(itemData)
            }
        }

        fun bindData(item: HashMap<String, Objects>) {
            Timber.d("bindData")
            itemData = item
            tv_title.text = item["title"].toString()
            tv_time.text = convertTime(item["time"].toString().toLong())
            val imageName = item["imageName"].toString()
            val pathUri = "file:///android_asset/workouts/$imageName"
            Glide.with(fragment.requireContext())
                .load(pathUri)
                .error(R.drawable.default_image)
                .into(iv_image)
        }

        private fun downloadImage(filename: String) {
            val islandRef = storageRef.child("images/workouts/bridge/$filename")
            val localFile = File.createTempFile("images", "jpg")
            islandRef.getFile(localFile).addOnSuccessListener {
                Timber.d(localFile.absolutePath)
                CacheImageManager.putImage(App.context!!, filename, localFile)
                val bitmap: Bitmap = BitmapFactory.decodeFile(localFile.absolutePath)
//                iv_image.setImageBitmap(bitmap)
//                val gifFromPath = pl.droidsonroids.gif.GifDrawable(localFile.absolutePath)
//                iv_image.setImageDrawable(gifFromPath)
                Glide.with(App.context!!)
                    .load(localFile)
                    .error(R.drawable.default_image)
                    .into(iv_image)
                Log.d(TAG, "success download image")
            }
                .addOnFailureListener { exception ->
                    Log.w(TAG, "Error.", exception)
                }
//            val frameAnimation = AnimationDrawable()
//            frameAnimation.isOneShot = false
//            islandRef.listAll().addOnSuccessListener { listResult ->
//                for (file in listResult.items) {
//                    val localFile = File.createTempFile("images", "jpg")
//                    file.getFile(localFile).addOnSuccessListener {
//                        val bitmap: Bitmap = BitmapFactory.decodeFile(localFile.absolutePath)
//                        val frame: BitmapDrawable = bitmap.toDrawable(fragment.resources)
//                        frameAnimation.addFrame(frame, 1000)
//                    }.addOnSuccessListener {
//                        iv_image.background = frameAnimation
//                        frameAnimation.start()
//                    }
//                }
//                Timber.d("success download image")
//            }
//                .addOnFailureListener { exception ->
//                    Timber.w(exception, "Error.")
//                }
        }

        @SuppressLint("SimpleDateFormat")
        private fun convertTime(time: Long): String {
            return SimpleDateFormat("mm:ss").format(Date(time))
        }
    }

    interface OnItemClickListener {
        fun onClick(map: HashMap<String, Objects>)
    }
}