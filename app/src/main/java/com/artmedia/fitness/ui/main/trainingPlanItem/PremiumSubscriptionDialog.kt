package com.artmedia.fitness.ui.main.trainingPlanItem

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.fragment.app.DialogFragment
import com.artmedia.fitness.ui.main.getPremium.PremiumActivity
import com.kulbaev.fitness.R
import kotlinx.android.synthetic.main.fragment_premium_subscription_dialog.*
import timber.log.Timber

class PremiumSubscriptionDialog : DialogFragment() {
    var listener: OnDismissCancelListener? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.GO_PREMIUM_DIALOG)
    }

    override fun onStart() {
        super.onStart()
        val d = dialog
        if (d != null) {
            val width = ViewGroup.LayoutParams.MATCH_PARENT
            val height = ViewGroup.LayoutParams.MATCH_PARENT
            d.window!!.setLayout(width, height)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_premium_subscription_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        go_premium.setOnClickListener {
            val intent = Intent(requireActivity(), PremiumActivity::class.java)
            startActivity(intent)
        }
        back.setOnClickListener {
            dismiss()
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        return dialog
    }

    override fun onDismiss(dialog: DialogInterface) {
        super.onDismiss(dialog)
        Timber.d("onDismiss")
        listener?.onDismiss(dialog)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if (context is OnDismissCancelListener) {
            listener = context
        }
    }

    interface OnDismissCancelListener {
        fun onDismiss(dialog: DialogInterface)
    }
}