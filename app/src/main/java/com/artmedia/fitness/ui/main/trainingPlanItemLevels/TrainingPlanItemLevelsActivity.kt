package com.artmedia.fitness.ui.main.trainingPlanItemLevels

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.artmedia.fitness.App
import com.artmedia.fitness.libs.LocaleHelper
import com.artmedia.fitness.ui.main.trainingPlanItem.TrainingPlanItemActivity
import com.bumptech.glide.Glide
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.kulbaev.fitness.R
import kotlinx.android.synthetic.main.activity_training_plan_item_levels.*
import kotlinx.android.synthetic.main.content_scrolling_item_level.*

class TrainingPlanItemLevelsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_training_plan_item_levels)
        setSupportActionBar(findViewById(R.id.toolbar_item_level))
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayShowHomeEnabled(true)
        findViewById<CollapsingToolbarLayout>(R.id.toolbar_layout).title = title
        setData()

        beginner.setOnClickListener {
            startTrainingItem("25", resources.getString(R.string.splits_training_beginner))
        }
        intermediate.setOnClickListener {
            startTrainingItem("26", resources.getString(R.string.splits_training_intermediate))
        }
        advanced.setOnClickListener {
            startTrainingItem("27", resources.getString(R.string.splits_training_advanced))
        }

        //hide title expanded
        var isShow = true
        var scrollRange = -1
        app_bar.addOnOffsetChangedListener(AppBarLayout.OnOffsetChangedListener { appBarLayout, verticalOffset ->
            if (scrollRange == -1) {
                scrollRange = appBarLayout?.totalScrollRange!!
            }
            if (scrollRange + verticalOffset == 0) {
                toolbar_layout.title = resources.getString(R.string.splits_training)
                isShow = true
            } else if (isShow) {
                toolbar_layout.title =
                    " " //careful there should a space between double quote otherwise it wont work
                isShow = false
            }
        })
    }

    private fun setData() {
        tv_title.text = resources.getString(R.string.splits_training)
        val pathUri = "file:///android_asset/homeImages/home16.jpg"
        Glide.with(App.context!!)
            .load(pathUri)
            .error(R.drawable.default_image)
            .into(target_image_view)
    }

    private fun startTrainingItem(trainingId: String, title: String) {
        App.pref.trainingItemId = trainingId
        val intent = Intent(this, TrainingPlanItemActivity::class.java)
        intent.putExtra("trainingPlanTitle", title)
        intent.putExtra("trainingPlanImageName", "home16.jpg")
        intent.putExtra("trainingPlanPremium", false)
        startActivity(intent)
    }

    override fun attachBaseContext(newBase: Context) {
        super.attachBaseContext(LocaleHelper.onAttach(newBase))
    }
}